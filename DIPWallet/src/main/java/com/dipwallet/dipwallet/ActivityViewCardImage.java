package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.File;


public class ActivityViewCardImage extends Activity {

    private MyApp myApp;
    private ImageView image_card_full;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApp) getApplicationContext();

        ActionBar action_bar = getActionBar();
        //action_bar.hide();

        setContentView(R.layout.activity_view_card_image);

        image_card_full = (ImageView) findViewById(R.id.image_card_full);
        showFullCardImage();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_view_card_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void showFullCardImage() {
        try {
//            String filename = (myApp.frontView) ? myApp.frontCardFilename : myApp.backCardFilename;
//            File file = new File(filename);
//            if (file.exists()) {
//                Bitmap bitmap = BitmapFactory.decodeFile(filename);
//                image_card_full.setImageBitmap(bitmap);
//            } else {
//                image_card_full.setImageDrawable(null);
//            }
            if (myApp.cardImage != null) {
                image_card_full.setImageBitmap(myApp.cardImage);
            } else {
                image_card_full.setImageDrawable(null);
            }

        } catch (Exception e) {

        }
    }
}
