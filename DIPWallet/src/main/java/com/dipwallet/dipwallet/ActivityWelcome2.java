package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.w3c.dom.Text;

public class ActivityWelcome2 extends Activity {

    private MyApp myApp;
    private Button button_join;
    private Button button_signin;

    private TextView title_text;
    private TextView slogan_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome3);
        myApp = (MyApp) getApplicationContext();

        ActionBar action_bar = getActionBar();
//        action_bar.hide();

        title_text = (TextView)findViewById(R.id.title_text);
        Typeface face = Typeface.createFromAsset(getAssets(), "preg.otf");
        title_text.setTypeface(face);

        button_join = (Button) findViewById(R.id.button_join);
        button_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegisterScreen();
            }
        });

        button_signin = (Button) findViewById(R.id.signInButton);
        button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSigninScreen();
            }
        });
    }
    private void showRegisterScreen() {
        try {
            Intent intent = new Intent(this, ActivityRegister.class);
            startActivity(intent);
//            finish();

        } catch (Exception e) {
            myApp.logE("Fail to show register screen", e);
        }
    }

    private void showSigninScreen() {
        try {
            Intent intent = new Intent(this, ActivitySignin.class);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            myApp.logE("Fail to show signin screen", e);
        }
    }
}
