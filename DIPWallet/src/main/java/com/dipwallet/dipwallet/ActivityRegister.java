package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ActivityRegister extends Activity {

    private Activity activity;
    private MyApp myApp;
    private EditText edit_register_name;
    private EditText edit_register_email;
    private EditText edit_register_mobile_phone;
//    private EditText edit_register_gender;

    private RadioGroup edit_register_gender;
    private RadioButton edit_radio_sex;

    private EditText edit_register_birthday;
    private EditText edit_register_address;
    private EditText edit_register_city;
//    private Button button_cancel_register;
    private Button button_register;
    private AsyncTaskXMPPClient task;

    private TextView troubleRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            activity = this;
            myApp = (MyApp) getApplicationContext();

            ActionBar action_bar = getActionBar();
//            action_bar.hide();

            setContentView(R.layout.activity_register);

            edit_register_name = (EditText) findViewById(R.id.edit_register_name);
            edit_register_email = (EditText) findViewById(R.id.edit_register_email);
            edit_register_mobile_phone = (EditText) findViewById(R.id.edit_register_mobile_phone);
//            edit_register_gender = (EditText) findViewById(R.id.edit_register_gender);
            edit_register_birthday = (EditText) findViewById(R.id.edit_register_birthday);
            edit_register_address = (EditText) findViewById(R.id.edit_register_address);
            edit_register_city = (EditText) findViewById(R.id.edit_register_city);
            edit_register_gender = (RadioGroup)findViewById(R.id.edit_register_gender);
            troubleRegister = (TextView)findViewById(R.id.text_not_registered);
            troubleRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickTroubleRegister();
                }
            });

//            button_cancel_register = (Button) findViewById(R.id.button_cancel_register);
//            button_cancel_register.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // Close me
//                    finish();
//                }
//            });

            button_register = (Button) findViewById(R.id.button_register);
            button_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    registerMe();
                }
            });

            // Test only
//            edit_register_name.setText("Made Budiarta");
//            edit_register_email.setText("made@datindo.co.id");
//            edit_register_mobile_phone.setText("081234567");
//            edit_register_gender.setText("M");
//            edit_register_birthday.setText("20010101");
//            edit_register_address.setText("Jl.Rasuna Said No 123");
//            edit_register_city.setText("Jakarta");
            // End of test

        } catch (Exception e) {
            myApp.logE("Fail on create register activity", e);
        }
    }
    private void registerMe() {
        try {
            // Name
            String name = edit_register_name.getText().toString();
            if (name.isEmpty()) {
                Toast.makeText(this, "Please enter name", Toast.LENGTH_SHORT).show();
                edit_register_name.requestFocus();
            }
            // Email
            String email = edit_register_email.getText().toString();
            if (email.isEmpty()) {
                Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
                edit_register_email.requestFocus();
            }
            // Mobile Phone
            String mobilePhone = edit_register_mobile_phone.getText().toString();
            if (mobilePhone.isEmpty()) {
                Toast.makeText(this, "Please enter mobile phone", Toast.LENGTH_SHORT).show();
                edit_register_mobile_phone.requestFocus();
            }
            // Gender
            int checked = edit_register_gender.getCheckedRadioButtonId();
            edit_radio_sex = (RadioButton)findViewById(checked);
            String gender = edit_radio_sex.getText().toString();
//            String gender = edit_register_gender.getText().toString();
            // Birthday
            String birthday = edit_register_birthday.getText().toString();
            // Address
            String address = edit_register_address.getText().toString();
            // City
            String city = edit_register_city.getText().toString();
            // Register data
            System.out.println(name + "," + gender + "," + birthday);
            myApp.myName = name;
            myApp.myEmail = email;
            myApp.myMobilePhone = mobilePhone;
            myApp.myGender = gender;
            myApp.myBirthday = birthday;
            myApp.myAddress = address;
            myApp.myCity = city;
            task = new AsyncTaskXMPPClient(activity, MyApp.TASK_ID_REGISTER);
            task.setOnTaskCompleteListener(new OnTaskCompleteListener() {
                @Override
                public void onTaskComplete(boolean result, String resultText) {
                    if (result) {
                        // Close me
                        finish();
                    }
                }
            });
            task.execute();

        } catch (Exception e) {
            myApp.logE("Fail to register me", e);
        }
    }

    private String getDeviceId() {
        try {
            TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            return mgr.getDeviceId();

        } catch (Exception e) {
            myApp.logE("Fail to get device id", e);
            return "";
        }
    }
    public void onClickTroubleRegister(){
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto"));
        intent.putExtra(Intent.EXTRA_EMAIL, "chir34@gmail.com");
        intent.putExtra(Intent.EXTRA_EMAIL, "Registration Issue Error[343]");
        startActivity(Intent.createChooser(intent, "Choose an email client from. . ."));
        /**
         * Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
         email.putExtra(Intent.EXTRA_EMAIL, recipients);
         email.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
         email.putExtra(Intent.EXTRA_TEXT, body.getText().toString());
         startActivity(Intent.createChooser(email, "Choose an email client from..."));
         */

    }
}
