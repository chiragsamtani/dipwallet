package com.dipwallet.dipwallet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class TrialMainActivity extends AppCompatActivity implements NavigationDrawerCallbacks {

    private MyApp myApp;
    private Activity activity;

    private FloatingActionButton buttonAddCard;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerAdapter pagerAdapter;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    private ImageView imageView;
    private AppBarLayout appBar;

    private String[] categories;

    private final String LOG_TAG = "ActivityMain";

    private static final String FASHION_PROMPT = "Fashion";
    private static final String FOOD_PROMPT = "Food";
    private static final String MEDICAL_PROMPT = "Medical";
    private static final String OTHERS_PROMPT = "Others";
    private static final String HOTELS_PROMPT = "Hotels";
    private static final String SPORTS_PROMPT = "Sports";

    private static final int FASHION_PAGE = 0;
    private static final int FOOD_PAGE = 1;
    private static final int MEDICAL_PAGE = 2;
    private static final int HOTELS_PAGE = 3;
    private static final int SPORTS_PAGE = 4;
    private static final int OTHERS_PAGE = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v5);
        myApp = (MyApp) getApplicationContext();
        activity = this;

        // set variables to respective layout
//        imageView = (ImageView) findViewById(R.id.backdrop);
        appBar = (AppBarLayout) findViewById(R.id.appbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        buttonAddCard = (FloatingActionButton) findViewById(R.id.fabButton);
        buttonAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCard();
            }
        });
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // initiate the toolbar and tabs
//        initToolbar();
        initViewPagerAndTabs();
//        loadBackdrop();
        tabLayout.setupWithViewPager(viewPager);

        // set up the drawer
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.setUserData("Chirag Samtani", "chir34@gmail.com", BitmapFactory.decodeResource(getResources(), R.drawable.new_avatar));

        // when things are moving
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                switch(position){
                    case FASHION_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.PinkCardColor));
                        buttonAddCard.setBackgroundTintList(getResources().getColorStateList(R.color.PinkCardColor));
                        appBar.setBackgroundColor(getResources().getColor(R.color.PinkCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.PinkCardColor));
                        // check your color to set the filter
//                        imageView.setColorFilter(Color.argb(100, 220, 20, 60));
                        break;
                    case FOOD_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.GreenCardColor));
                        buttonAddCard.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.GreenCardColor)));
                        appBar.setBackgroundColor(getResources().getColor(R.color.GreenCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.GreenCardColor));
                        break;
                    case MEDICAL_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.RedCardColor));
                        buttonAddCard.setBackgroundTintList(getResources().getColorStateList(R.color.RedCardColor));
                        appBar.setBackgroundColor(getResources().getColor(R.color.RedCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.RedCardColor));
                        break;
                    case HOTELS_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.BlueCardColor));
                        buttonAddCard.setBackgroundTintList(getResources().getColorStateList(R.color.BlueCardColor));
                        appBar.setBackgroundColor(getResources().getColor(R.color.BlueCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.BlueCardColor));
                        break;
                    case SPORTS_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.OrangeCardColor));
                        buttonAddCard.setBackgroundTintList(getResources().getColorStateList(R.color.OrangeCardColor));
                        appBar.setBackgroundColor(getResources().getColor(R.color.OrangeCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.OrangeCardColor));
                        break;
                    case OTHERS_PAGE:
                        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.YellowCardColor));
                        buttonAddCard.setBackgroundTintList(getResources().getColorStateList(R.color.YellowCardColor));
                        appBar.setBackgroundColor(getResources().getColor(R.color.YellowCardColor));
                        mToolbar.setBackgroundColor(getResources().getColor(R.color.YellowCardColor));
                        break;
                }
            }
        });
    }

    private void initViewPagerAndTabs() {
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        // set the string to retrieve data from datackse
        categories = getResources().getStringArray(R.array.category);
        String FASHION = myApp.myCardDb.getCardsByCategoryQuery(categories[FASHION_PAGE]);
        String FOOD = myApp.myCardDb.getCardsByCategoryQuery(categories[FOOD_PAGE]);
        String MEDICAL = myApp.myCardDb.getCardsByCategoryQuery(categories[MEDICAL_PAGE]);
        String HOTELS = myApp.myCardDb.getCardsByCategoryQuery(categories[HOTELS_PAGE]);
        String SPORTS = myApp.myCardDb.getCardsByCategoryQuery(categories[SPORTS_PAGE]);
        String OTHERS = myApp.myCardDb.getCardsByCategoryQuery(categories[OTHERS_PAGE]);

        // create all the pageAdapter a.k.a every tab + its content -- ADD MUST BE IN THIS ORDER
        pagerAdapter.addFragment(ListViewFragment.createInstance(FASHION), categories[FASHION_PAGE]);
        pagerAdapter.addFragment(ListViewFragment.createInstance(FOOD), categories[FOOD_PAGE]);
        pagerAdapter.addFragment(ListViewFragment.createInstance(MEDICAL), categories[MEDICAL_PAGE]);
        pagerAdapter.addFragment(ListViewFragment.createInstance(HOTELS), categories[HOTELS_PAGE]);
        pagerAdapter.addFragment(ListViewFragment.createInstance(SPORTS), categories[SPORTS_PAGE]);
        pagerAdapter.addFragment(ListViewFragment.createInstance(OTHERS), categories[OTHERS_PAGE]);

        // set adapter to the view pager
        viewPager.setAdapter(pagerAdapter);

        // set Tabs with the desired content
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        final Intent intent;
        switch(position){
            case 0:
                addCard();
                break;
            case 1:
                Toast.makeText(getApplicationContext(), "Feature no. " + position + " hasn't been implemented yet", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(getApplicationContext(), "Feature no. " + position + " hasn't been implemented yet", Toast.LENGTH_SHORT ).show();
                break;
            case 3:
                String phone = "+6287821124544";
                intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            case 4:
                signOut();
                break;
            default:
                break;

        }
    }

    // page adapter class
    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            // get the fragment / ListView / Recycle View
            fragmentList.add(fragment);
            // Name of the tab
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    // refresh adapter
    public void refreshThisPageAdapter() {
        int page_number = viewPager.getCurrentItem();
        ListViewFragment current_page = (ListViewFragment) pagerAdapter.getItem(page_number);
        MyListCursorAdapter current_adapter = current_page.recyclerAdapter;
        if (current_adapter != null) {
            current_page.recyclerAdapter.refresh();
            current_page.setupRecyclerView(current_page.recyclerView);
        }
    }

    // refresh other page adapter
    public void refreshThatPageAdapter(int page_number) {
            ListViewFragment that_page = (ListViewFragment) pagerAdapter.getItem(page_number);
            MyListCursorAdapter current_adapter = that_page.recyclerAdapter;
            if (current_adapter != null) {
                that_page.recyclerAdapter.refresh();
                that_page.setupRecyclerView(that_page.recyclerView);
            }
    }

    // Delete Card Method
    public void onClickDeleteCard(View view) {
        try {
            final int card_id = Integer.valueOf(view.getTag().toString());

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.delete_card_confirm);
            builder.setMessage(R.string.delete_card_action);
            builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (myApp.myCardDb.deleteCard(card_id)) {
                        refreshThisPageAdapter();
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.create().show();

        } catch (Exception e) {
            logE("Fail on click delete card", e);
        }
    }

    // Add Card Method
    public void addCard() {
        try {
            Intent intent = new Intent(this, ActivityAddCard.class);
            startActivityForResult(intent, MyApp.REQ_CODE_ADD_CARD);
            System.out.println("Add Card");
        } catch (Exception e) {
            logE("Fail on add Card", e);
        }
    }

    // Edit Card Method
    public void onClickEditCard(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            Intent intent = new Intent(this, ActivityEditCard.class);
            intent.putExtra("value1", card_id);
            startActivityForResult(intent, MyApp.TASK_ID_UPDATE_DATA);
            System.out.println("On Click Edit Card");
//            myApp.cardData = myApp.myCardDb.getCardData(card_id);
//            String msg = String.format("Edit Card ID : %d", card_id);
//            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            logE("Fail on click edit card", e);
        }
    }


    // method to view card's thumbnail when adapter's picture is clicked
    public void onClickViewCardImage(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            myApp.cardImage = myApp.myCardDb.getFrontImage(card_id);
            Intent intent = new Intent(this, ActivityViewCardImage.class);
            startActivity(intent);
        } catch (Exception e) {
            logE("Fail on click view card image", e);
        }
    }

    // method to view card's data when adapter is clicked
    public void onClickViewCardData(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            myApp.cardData = myApp.myCardDb.getCardData(card_id);
            Intent intent = new Intent(this, ActivityViewCard.class);
            startActivity(intent);

        } catch (Exception e) {
            logE("Fail on click view card", e);
        }
    }

    // notification on add/edit data + adapter being updated
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == MyApp.REQ_CODE_ADD_CARD) {
                boolean success = data.getBooleanExtra("success", false);
                String page = data.getStringExtra("category");
                int page_number = whichPage(page);
                if (success) {
                    refreshThatPageAdapter(page_number);
                    AlertDialog.Builder addBuilder = new AlertDialog.Builder(this);
                    addBuilder.setTitle("Add Card");
                    addBuilder.setMessage("Card has been successfully added");
                    addBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.out.println("Ok");
                        }
                    });
                    AlertDialog addDialog = addBuilder.create();
                    addDialog.show();

                }
            }
            else if(resultCode == MyApp.TASK_ID_UPDATE_DATA){
                boolean success = data.getBooleanExtra("success", false);
                String page = data.getStringExtra("category");
                int page_number = whichPage(page);
                if(success){
                    refreshThisPageAdapter();
                    if (viewPager.getCurrentItem() != page_number) {
                        refreshThatPageAdapter(page_number);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Edit Card");
                    builder.setMessage("Card Successfully Edited");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.out.println("Ok");
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            }

        } catch (Exception e) {
            myApp.logE("Fail on get activity result", e);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public int whichPage(String page_name){
        switch (page_name){
            case FASHION_PROMPT:
                return FASHION_PAGE;

            case FOOD_PROMPT:
                return FOOD_PAGE;

            case MEDICAL_PROMPT:
                return MEDICAL_PAGE;

            case HOTELS_PROMPT:
                return HOTELS_PAGE;

            case SPORTS_PROMPT:
                return SPORTS_PAGE;

            case OTHERS_PROMPT:
                return OTHERS_PAGE;
        }
        return OTHERS_PAGE;
    }

    public void signOut(){

        try{
            AlertDialog.Builder addBuilder = new AlertDialog.Builder(this);
            addBuilder.setTitle("Sign Out");
            addBuilder.setMessage("Are you sure you want to sign out?");
            addBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(TrialMainActivity.this, ActivitySignin.class);
                    startActivity(intent);
                }
            });
            addBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            AlertDialog addDialog = addBuilder.create();
            addDialog.show();
        }catch(Exception e){}
    }

    // log Method - to record any message / error
    private void logE(String msg, Exception e) {
        String error = (e.getMessage() == null) ? "" : e.getMessage();
        Log.e(LOG_TAG, String.format("%s: %s", msg, error));
    }

    private void loadBackdrop() {
        Glide.with(this).load(R.drawable.ic_fitness).centerCrop().into(imageView);
    }
}