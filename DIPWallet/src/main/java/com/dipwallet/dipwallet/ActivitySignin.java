package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class ActivitySignin extends Activity {

    private Activity activity;
    private MyApp myApp;
    private EditText edit_signin_user_id;
    private EditText edit_signin_password;
    private Button button_cancel_signin;
    private Button button_signin2;
    private TextView text_forgot_password;
    private TextView text_not_registered;
    private AsyncTaskXMPPClient task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        myApp = (MyApp) getApplicationContext();

//        ActionBar action_bar = getActionBar();
//        action_bar.hide();

        setContentView(R.layout.activity_signin);

        edit_signin_user_id = (EditText) findViewById(R.id.edit_signin_user_id);
        edit_signin_password = (EditText) findViewById(R.id.edit_signin_password);

        button_cancel_signin = (Button) findViewById(R.id.button_cancel_signin);
        button_cancel_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Close me
                finish();
            }
        });

        button_signin2 = (Button) findViewById(R.id.button_signin2);
        button_signin2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSignin();
            }
        });

        text_forgot_password = (TextView) findViewById(R.id.text_forgot_password);
        text_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickForgotPassword();
            }
        });
        text_not_registered = (TextView) findViewById(R.id.text_not_registered);
        text_not_registered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNotRegistered();
            }
        });

        // Test only
        edit_signin_user_id.setText("datindo.co.id.made");
        edit_signin_password.setText("ZHNE1C");
        // End of test
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_signin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void userSignin() {
        try {
            // User Id
            String userId = edit_signin_user_id.getText().toString();
            String password = edit_signin_password.getText().toString();
            if (userId.isEmpty()||password.isEmpty()) {
                Toast.makeText(this, "Please enter user id", Toast.LENGTH_SHORT).show();
                edit_signin_user_id.requestFocus();
                edit_signin_password.requestFocus();
            }
            // Password
//            String password = edit_signin_password.getText().toString();
//            if (password.isEmpty()) {
//                Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
//                edit_signin_password.requestFocus();
//            }
            // Signin
            myApp.myUserId = userId;
            myApp.myPassword = password;
            task = new AsyncTaskXMPPClient(this, MyApp.TASK_ID_SIGNIN);
            task.setOnTaskCompleteListener(new OnTaskCompleteListener() {
                @Override
                public void onTaskComplete(boolean result, String resultText) {
                    if (result) {
                        // Close me
                        finish();
                    }
                    // Start main activity
                    Intent intent = new Intent(activity, ActivityMain.class);
                    startActivity(intent);

                }
            });
            task.execute();

        } catch (Exception e) {
            myApp.logE("Fail to signin", e);
        }
    }


    private void onClickForgotPassword() {
        try {
            // Start forgot password activity
            Intent intent = new Intent(activity, ActivityForgotPassword.class);
            startActivity(intent);

        } catch (Exception e) {
            myApp.logE("Fail on click forgot password", e);
        }
    }

    private void onClickNotRegistered() {
        try {
            // Start not registered activity
            Intent intent = new Intent(activity, ActivityRegister.class);
            startActivity(intent);

        } catch (Exception e) {
            myApp.logE("Fail on click not registered", e);
        }
    }
}
