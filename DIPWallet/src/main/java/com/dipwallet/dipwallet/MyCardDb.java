package com.dipwallet.dipwallet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Made Budiarta on 2/26/2015.
 */
public class MyCardDb extends SQLiteOpenHelper {

    private final String LOG_TAG = "MyCardDb";
    private final String TABLE_NAME = "card";
    private String dbFilename;

    public MyCardDb(Context context, String db_filename, int db_version) {
        super(context, db_filename, null, db_version);
        dbFilename = db_filename;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {

    }

    private void logE(String msg, Exception e) {
        String error = (e.getMessage() == null) ? "" : e.getMessage();
        Log.e(LOG_TAG, String.format("%s: %s", msg, error));
    }

    private void createDb(SQLiteDatabase db) {
        try {
            // Create table system_log
            String sql = "CREATE TABLE IF NOT EXISTS card ( ";
            sql += "card_id INT PRIMARY KEY, ";
            sql += "preview_image BLOB, ";
            sql += "front_image BLOB, ";
            sql += "back_image BLOB, ";
            sql += "card_title TEXT, ";
            sql += "card_no TEXT, ";
            sql += "cardholder_name TEXT, ";
            sql += "card_note TEXT, ";
            /**added**/sql += "category TEXT, ";
            sql += "created_date BIGINT, ";
            sql += "modified_date BIGINT )";
            db.execSQL(sql);

        } catch (Exception e) {
            logE("Fail to create database", e);
        }
    }

    private SQLiteDatabase openDb() {
        try {
            return SQLiteDatabase.openDatabase(dbFilename, null,
                    SQLiteDatabase.OPEN_READWRITE);

        } catch (Exception e) {
            logE(String.format("Fail to open database '%s'", dbFilename), e);
            return null;
        }
    }

    public ArrayList<String> getCardTitles() {
        try {
            ArrayList<String> card_desc_list = new ArrayList<String>();
            card_desc_list.add("ACE Rewards");
            card_desc_list.add("LOTTE Members");
            card_desc_list.add("STARBUCKS CARD");
            card_desc_list.add("MySTAR Card member");
            card_desc_list.add("Informa Rewards");
            card_desc_list.add("MCC - Matahari Club Card");
            card_desc_list.add("RS MITRA KELUARGA");
            card_desc_list.add("RS PONDOK INDAH");
            card_desc_list.add("RS Metropolitan Medical Centre");
            card_desc_list.add("HERMINA HOSPITAL GROUP");
            card_desc_list.add("One Heart Card – Honda Card");
            return card_desc_list;

        } catch (Exception e) {
            return new ArrayList<String>();
        }
    }


    private int getNewCardId() {
        try {
            int card_id = 1;

            SQLiteDatabase db = openDb();
            String sql = "SELECT MAX(card_id) FROM card";
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int max_card_id = cursor.getInt(0);
                card_id = max_card_id + 1;
            }
            db.close();
            //added
            System.out.println("Reaches Here 1");
            cursor.close();
            return card_id;

        } catch (Exception e) {
            logE("Fail to get new card id", e);
            return 1;
        }
    }

    //new method for update
    public boolean updateCard(MyApp.CardData cData){
        try{

            long current_time = new Date().getTime();
            SQLiteDatabase db = openDb();
            ContentValues values = new ContentValues();

            int card_id = cData.cardId;

            values.put("preview_image", getBytes(cData.previewImage));
            values.put("front_image", getBytes(cData.frontImage));
            values.put("back_image", getBytes(cData.backImage));
            values.put("card_title", cData.cardTitle);
            values.put("card_no", cData.cardNo);
            values.put("cardholder_name", cData.cardholderName);
            values.put("card_note", cData.cardNote);
            /**added**/ values.put("category", cData.category);
            values.put("created_date", current_time);
            values.put("modified_date", current_time);
            db.beginTransaction();

            try{
                db.update("card", values, "card_id = ? ", new String[]{String.valueOf(card_id)});
                db.setTransactionSuccessful();
            }
            finally{
                db.endTransaction();
                db.close();
            }

            return true;
        }catch(Exception e){
            logE("Fail to update card", e);
            return false;
        }

    }

    public boolean addCard(MyApp.CardData card_data) {
        try {
            long current_time = new Date().getTime();

            SQLiteDatabase db = openDb();
            ContentValues values = new ContentValues();
            values.put("card_id", getNewCardId());
            values.put("preview_image", getBytes(card_data.previewImage));
            values.put("front_image", getBytes(card_data.frontImage));
            values.put("back_image", getBytes(card_data.backImage));
            values.put("card_title", card_data.cardTitle);
            values.put("card_no", card_data.cardNo);
            values.put("cardholder_name", card_data.cardholderName);
            values.put("card_note", card_data.cardNote);
            /**added**/values.put("category", card_data.category);
            values.put("created_date", current_time);
            values.put("modified_date", current_time);
            db.beginTransaction();
            try {
                db.insert("card", "", values);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
                db.close();
            }

            System.out.println("Reaches Here 3");
            return true;

        } catch (Exception e) {
            logE("Fail to add card", e);
            return false;
        }
    }

    private static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        }
        return stream.toByteArray();
    }

    public String getCardsQuery() {
        String sql = "SELECT rowid _id, card_id, front_image, card_title, card_no, cardholder_name, category FROM card;";
        return sql;
    }

    public String getCardsByCategoryQuery(String givenCategory) {
        String sql = "SELECT rowid _id, card_id, front_image, card_title, card_no, cardholder_name, category FROM " + TABLE_NAME +" WHERE " + "category" + " = '"+ givenCategory + "';";
        return sql;
    }

    public Cursor getCardsCursor(String sql)
    {
        try {
            SQLiteDatabase db = openDb();
            Cursor cursor = db.rawQuery(sql, null);
            System.out.println("Reaches Here 4");
            return (cursor.moveToNext()) ? cursor : null;
        } catch (Exception e) {
            logE("Fail to get cards cursor", e);
            return null;
        }

    }

    public Bitmap getFrontImage(int card_id) {
        try {
            Bitmap bitmap = null;
            SQLiteDatabase db = openDb();
            String sql = String.format("SELECT front_image FROM card WHERE card_id = %d", card_id);
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                byte[] blob = cursor.getBlob(0);
                ByteArrayInputStream stream = new ByteArrayInputStream(blob);
                bitmap = BitmapFactory.decodeStream(stream);
            }
            cursor.close();
            db.close();
            System.out.println("Reaches Here 5");
            return bitmap;

        } catch (Exception e) {
            logE("Fail to get front image", e);
            return null;
        }
    }
    public boolean deleteCard(int card_id) {
        try {
            String where = "card_id = " + String.valueOf(card_id);
            SQLiteDatabase db = openDb();
            int count = db.delete("card", where, null);
            boolean result = (count > 0) ? true : false;
            db.close();
            return result;

        } catch (Exception e) {
            logE("Fail to delete card", e);
            return false;
        }
    }

    public MyApp.CardData getCardData(int card_id) {
        try {
            MyApp.CardData card_data = new MyApp.CardData();
            /**added in sql+= category**/
            String sql = "SELECT preview_image, front_image, back_image, card_title, card_no, ";
            sql += "cardholder_name, card_note, category, created_date, modified_date FROM card ";
            sql += String.format("WHERE card_id = %d", card_id);
            SQLiteDatabase db = openDb();
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                card_data.cardId = card_id;
                byte[] blob = cursor.getBlob(0);
                ByteArrayInputStream stream = new ByteArrayInputStream(blob);
                card_data.previewImage = BitmapFactory.decodeStream(stream);
                blob = cursor.getBlob(1);
                stream = new ByteArrayInputStream(blob);
                card_data.frontImage = BitmapFactory.decodeStream(stream);
                blob = cursor.getBlob(2);
                stream = new ByteArrayInputStream(blob);
                card_data.backImage = BitmapFactory.decodeStream(stream);
                card_data.cardTitle = cursor.getString(3);
                card_data.cardNo = cursor.getString(4);
                card_data.cardholderName = cursor.getString(5);
                card_data.cardNote = cursor.getString(6);
                card_data.category = cursor.getString(7);
                card_data.createdDate = new Date(cursor.getLong(8));
                card_data.modifiedDate = new Date(cursor.getLong(9));
            }
            cursor.close();
            System.out.println("Reaches Here 6");
            db.close();
            return card_data;

        } catch (Exception e) {
            return new MyApp.CardData();
        }
    }
}