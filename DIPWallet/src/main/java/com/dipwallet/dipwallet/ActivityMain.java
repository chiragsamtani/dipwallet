package com.dipwallet.dipwallet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.gc.materialdesign.views.ButtonFloat;


public class ActivityMain extends ActionBarActivity
        implements NavigationDrawerCallbacks {
    private final String LOG_TAG = "ActivityMain";
    private Context myContext;
    private Activity activity;
    private MyApp myApp;
    private ListView listCardItem;
    private FloatingActionButton buttonAddCard;
    private FrameLayout frameLayout;

    private ImageButton button_fashion;
    private ImageButton button_food;
    private ImageButton button_medical;
    private ImageButton button_hotels;
    private ImageButton button_sports;
    private ImageButton button_others;

    private ImageButton currentButton;
    private ImageButton previousButton;
    private TextView currentText;
    private TextView previousText;

    private TextView text_fashion;
    private TextView text_food;
    private TextView text_medical;
    private TextView text_hotels;
    private TextView text_sports;
    private TextView text_others;
    private TextView results_text;
    private Intent intent2;
    public AdapterCardItem adapterCardItem = null;


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private static final String FASHION_PROMPT = "Fashion";
    private static final String FOOD_PROMPT = "Food";
    private static final String MEDICAL_PROMPT = "Medical";
    private static final String OTHERS_PROMPT = "Others";
    private static final String HOTELS_PROMPT = "Hotels";
    private static final String SPORTS_PROMPT = "Sports";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v3);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        final Activity activity = this;

        myApp = (MyApp) getApplicationContext();
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

//         Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        updateProfile();

        listCardItem = (ListView) findViewById(R.id.list_card_item);
        listCardItem.setChoiceMode(AbsListView.CHOICE_MODE_NONE);


//      ASSIGNING BUTTONS
        button_fashion = (ImageButton)findViewById(R.id.fashion_button);
        button_food = (ImageButton)findViewById(R.id.food_button);
        button_medical = (ImageButton)findViewById(R.id.medical_button);
        button_hotels = (ImageButton)findViewById(R.id.hotels_button);
        button_sports = (ImageButton)findViewById(R.id.sports_button);
        button_others = (ImageButton)findViewById(R.id.others_button);
        buttonAddCard = (FloatingActionButton) findViewById(R.id.button_add_card);

//      ASSIGNING TEXTS
        text_fashion = (TextView)findViewById(R.id.fashion_text);
        text_food = (TextView)findViewById(R.id.food_text);
        text_medical = (TextView)findViewById(R.id.medical_text);
        text_hotels = (TextView)findViewById(R.id.hotels_text);
        text_others = (TextView)findViewById(R.id.others_text);
        text_sports = (TextView)findViewById(R.id.sports_text);

        results_text = (TextView)findViewById(R.id.empty_view);

        currentButton = null;
        previousButton = null;
        currentText = null;
        previousText = null;

////        FOR THE BEGINNING!
        //Before you click any button, the list displays all the cards that has been added
        //this is during startup of the app(after sign in)
        String sql = myApp.myCardDb.getCardsQuery();
        if (retrieveCardsByCategory(sql) == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }

        //cases during category clicks
        //use method signature instead(fashion prompt, food prompt etc)
        //because we have onClick Listener on LinearLayout of the Button as well
        //to improve the senstivity of the button
        button_fashion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fashionPrompt(button_fashion);
            }
        });

        button_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodPrompt(button_food);
            }
        });

        button_medical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medicalPrompt(button_medical);
            }
        });

        button_hotels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hotelsPrompt(button_hotels);
            }
        });

        button_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sportsPrompt(button_sports);
            }

        });

        button_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                othersPrompt(button_others);
            }
        });

        buttonAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCard();
            }
        });

        ImageButton button5 = (ImageButton)findViewById(R.id.coba_button);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToTrial();
            }
        });

    }

    private void goToTrial(){
        Intent intent = new Intent(this, TrialMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        final Intent intent;
        switch (position) {
            case 0:
                addCard();
                break;
            case 1:
                Toast.makeText(getApplicationContext(), "Feature no. " + position + " hasn't been implemented yet", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(getApplicationContext(), "Feature no. " + position + " hasn't been implemented yet", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                String phone = "+6287821124544";
                intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            case 4:
                signOut();
                break;
            default:
                break;

        }

    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.activity_main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        item.setIcon(R.drawable.ic_notif);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

//        switch (keyCode) {
//            case KeyEvent.KEYCODE_MENU:
//                View v = findViewById(R.id.action_main_more);
//                v.performClick();
//                return true;
//        }

        return super.onKeyUp(keyCode, event);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == MyApp.REQ_CODE_ADD_CARD) {
                boolean success = data.getBooleanExtra("success", false);
                int card_count = data.getIntExtra("card_count", 0);
                if (success || card_count > 0) {
                    adapterCardItem.refresh();
                    AlertDialog.Builder addBuilder = new AlertDialog.Builder(this);
                    addBuilder.setTitle("Add Card");
                    addBuilder.setMessage("Card has been successfully added");
                    addBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.out.println("Ok");
                        }
                    });
                    AlertDialog addDialog = addBuilder.create();
                    addDialog.show();

                }
            } else if (resultCode == MyApp.TASK_ID_UPDATE_DATA) {
                boolean success = data.getBooleanExtra("success", false);
                if (success) {
                    adapterCardItem.refresh();
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Edit Card");
                    builder.setMessage("Card Successfully Edited");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.out.println("Ok");
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            }
            else if(resultCode == MyApp.TASK_UPDATE_PROFILE){
                boolean success = data.getBooleanExtra("success", false);
                if (success) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Edit Profile");
                    builder.setMessage("Profile Successfully Modified");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            System.out.println("Ok");
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    updateProfile();
                }
            }

        } catch (Exception e) {
            myApp.logE("Fail on get activity result", e);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClickEditCard(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            Intent intent = new Intent(this, ActivityEditCard.class);
            intent.putExtra("value1", card_id);
            startActivityForResult(intent, MyApp.TASK_ID_UPDATE_DATA);

        } catch (Exception e) {
            logE("Fail on click edit card", e);
        }
    }

    public void onClickDeleteCard(View view) {
        try {
            final int card_id = Integer.valueOf(view.getTag().toString());

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.delete_card_confirm);
            builder.setMessage(R.string.delete_card_action);
            builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
//                    String msg = String.format("Delete Card ID : %d", card_id);
//                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    if (myApp.myCardDb.deleteCard(card_id)) {
                        adapterCardItem.refresh();
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.create().show();

        } catch (Exception e) {
            logE("Fail on click delete card", e);
        }
    }

    public void onClickViewCardImage(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            myApp.cardImage = myApp.myCardDb.getFrontImage(card_id);
            Intent intent = new Intent(this, ActivityViewCardImage.class);
            startActivity(intent);

        } catch (Exception e) {
            logE("Fail on click view card image", e);
        }
    }

    public void onClickViewCardData(View view) {
        try {
            int card_id = Integer.valueOf(view.getTag().toString());
            myApp.cardData = myApp.myCardDb.getCardData(card_id);
            Intent intent = new Intent(this, ActivityViewCard.class);
            startActivity(intent);

        } catch (Exception e) {
            logE("Fail on click view card", e);
        }
    }

    private void logE(String msg, Exception e) {
        String error = (e.getMessage() == null) ? "" : e.getMessage();
        Log.e(LOG_TAG, String.format("%s: %s", msg, error));
    }

    public void addCard() {
        try {
            Intent intent = new Intent(this, ActivityAddCard.class);
            startActivityForResult(intent, MyApp.REQ_CODE_ADD_CARD);
            //adapterCardItem.refresh();
        } catch (Exception e) {
            logE("Fail on add Card", e);
        }
    }
    public void signOut(){

        try{
            AlertDialog.Builder addBuilder = new AlertDialog.Builder(this);
            addBuilder.setTitle("Sign Out");
            addBuilder.setMessage("Are you sure you want to sign out?");
            addBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    intent2 = new Intent(ActivityMain.this, ActivitySignin.class);
                    startActivity(intent2);
                }
            });
            addBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            AlertDialog addDialog = addBuilder.create();
            addDialog.show();
        }catch(Exception e){}
    }

    public Cursor retrieveCardsByCategory(String sql){
        Cursor cursor = null;
        if (!sql.isEmpty()) {
            cursor = myApp.myCardDb.getCardsCursor(sql);
            if (cursor != null) {
                adapterCardItem = new AdapterCardItem(this, cursor, sql);
                listCardItem.setAdapter(adapterCardItem);
                adapterCardItem.refresh();
            }

        }
        return cursor;
    }
    public void fashionPrompt(View v) {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_fashion;
        currentText = text_fashion;

        if (previousButton != null) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }


        currentButton.setColorFilter(getResources().getColor(R.color.FashionButton));
        currentText.setTextColor(getResources().getColor(R.color.FashionButton));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.FashionButton));

//                turning off the previous setting

        /**On-Click Fashion Changes SQL Query**/
        String FASHION = myApp.myCardDb.getCardsByCategoryQuery(FASHION_PROMPT);
        Cursor cursor = retrieveCardsByCategory(FASHION);
        if (cursor == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }
    }

    public void foodPrompt(View v) {

        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_food;
        currentText = text_food;


//                turning off the previous setting
        if (previousButton != null) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }

        button_food.setColorFilter(getResources().getColor(R.color.FoodButton));
        text_food.setTextColor(getResources().getColor(R.color.FoodButton));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.FoodButton));


        /**on-Click Food changes SQL Query to Food**/
        String FOOD = myApp.myCardDb.getCardsByCategoryQuery(FOOD_PROMPT);
        Cursor cursor = retrieveCardsByCategory(FOOD);
        if (cursor == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }
    }

    public void medicalPrompt(View v) {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_medical;
        currentText = text_medical;


//                turning off the previous setting
        if (previousButton != null) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }


        button_medical.setColorFilter(getResources().getColor(R.color.MedicalButton));
        text_medical.setTextColor(getResources().getColor(R.color.MedicalButton));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.MedicalButton));

        String MEDICAL = myApp.myCardDb.getCardsByCategoryQuery(MEDICAL_PROMPT);
        Cursor cursor = retrieveCardsByCategory(MEDICAL);
        if (cursor == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }
    }
    public void hotelsPrompt(View v){
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_hotels;
        currentText = text_hotels;


//                turning off the previous setting
        if (previousButton != null ) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }


        button_hotels.setColorFilter(getResources().getColor(R.color.BlueCardColor));
        text_hotels.setTextColor(getResources().getColor(R.color.BlueCardColor));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.BlueCardColor));



        String HOTELS = myApp.myCardDb.getCardsByCategoryQuery(HOTELS_PROMPT);
        Cursor cursor = retrieveCardsByCategory(HOTELS);
        if(cursor == null){
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        }
        else{
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }

    }
    public void sportsPrompt(View v){
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_sports;
        currentText = text_sports;


//                turning off the previous setting
        if (previousButton != null) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }

        button_sports.setColorFilter(getResources().getColor(R.color.OrangeCardColor));
        text_sports.setTextColor(getResources().getColor(R.color.OrangeCardColor));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.OrangeCardColor));


        String SPORTS = myApp.myCardDb.getCardsByCategoryQuery(SPORTS_PROMPT);
        Cursor cursor = retrieveCardsByCategory(SPORTS);
        if (cursor == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }
    }
    public void othersPrompt(View v){

        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();

        previousButton = currentButton;
        previousText = currentText;

        currentButton = button_others;
        currentText = text_others;

//                turning off the previous setting
        if (previousButton != null) {
            previousButton.setColorFilter(getResources().getColor(R.color.UntouchedButton));
            previousText.setTextColor(getResources().getColor(R.color.UntouchedButton));
        }

        button_others.setColorFilter(getResources().getColor(R.color.YellowCardColor));
        text_others.setTextColor(getResources().getColor(R.color.YellowCardColor));
        buttonAddCard.setBackgroundColor(getResources().getColor(R.color.YellowCardColor));

        String OTHERS = myApp.myCardDb.getCardsByCategoryQuery(OTHERS_PROMPT);
        Cursor cursor = retrieveCardsByCategory(OTHERS);
        if (cursor == null) {
            listCardItem.setVisibility(View.INVISIBLE);
            results_text.setVisibility(View.VISIBLE);
        } else {
            listCardItem.setVisibility(View.VISIBLE);
            results_text.setVisibility(View.INVISIBLE);
        }
    }


    public void updateProfile(){
        //reupdating myApp after change in global names and email!
        myApp = (MyApp)getApplicationContext();
        Bitmap avatar = myApp.avatarImage;
        String name = myApp.myName;
        String email = myApp.myEmail;
        if(avatar == null){
            avatar = BitmapFactory.decodeResource(getResources(), R.drawable.new_avatar);
        }
        mNavigationDrawerFragment.setUserData(name, email, avatar);
        System.out.println(myApp.myName + myApp.myEmail + myApp.myMobilePhone);
    }

    //added when a user clicks on the red box on the navigation drawer
    //to view their profile
    public void onProfileClick(View v){
        try {
            Intent intent = new Intent(this, ActivityViewProfile.class);
            startActivityForResult(intent, myApp.TASK_UPDATE_PROFILE);
        }catch (Exception e){
            logE("Fail on add Card", e);
        }
    }
}