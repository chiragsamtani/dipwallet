package com.dipwallet.dipwallet;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;

/**
 * Created by Chirag on 7/13/2015.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {


    private static final int HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int LISTITEM = 1;
    private static String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
    private static int mIcons[];       // Int Array to store the passed icons resource value from MainActivity.java

    private static String name;        //String Resource for header View Name
    private static String email;       //String Resource for header view email


    public static class ViewHolder extends RecyclerView.ViewHolder{

        int HolderId;

        TextView textView;
        ImageView imageView;
        TextView name;
        TextView email;
        public ViewHolder(View newView, int itemView){
            super(newView);
            if(itemView == LISTITEM) {
                textView = (TextView) newView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from item_row.xml
                imageView = (ImageView) newView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from item_row.xml
                HolderId = 1;                                               // setting holder id as 1 as the object being populated are of type item row
            }
            else{


                name = (TextView) newView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) newView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                HolderId = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
            }
        }


        }
    DrawerAdapter(String Titles[],int Icons[],String Name,String Email) { // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;                //have seen earlier
        mIcons = Icons;
        name = Name;
        email = Email;
        //in adapter

    }
    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
        @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            if(i == HEADER){
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header,viewGroup,false);
                ViewHolder vhItem = new ViewHolder(v, i);
                return vhItem;
            }
            else if(i == LISTITEM){
                View a = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row,viewGroup,false);
                ViewHolder listrow = new ViewHolder(a, i);
                return listrow;
            }
            return null;
    }

    @Override
    public int getItemCount() {
        return mNavTitles.length+1;
    }

    @Override
    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    public void onBindViewHolder(DrawerAdapter.ViewHolder viewHolder, int i) {
        if(viewHolder.HolderId == 1)
        {
            viewHolder.textView.setText(mNavTitles[i - 1]); // Setting the Text with the array of our Titles
            viewHolder.imageView.setImageResource(mIcons[i -1]);// Settimg the image with array of our
        }
        else{
            viewHolder.name.setText(name);
            viewHolder.email.setText(email);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return HEADER;
        else
            return LISTITEM;
    }
}