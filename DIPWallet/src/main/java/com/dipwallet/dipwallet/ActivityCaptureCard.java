package com.dipwallet.dipwallet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class ActivityCaptureCard extends ActionBarActivity implements SurfaceHolder.Callback, Camera.ShutterCallback  {

    MyApp myApp;
    Activity activity;
    private Camera camera;
    SurfaceView surfaceView;
    ImageView guidedFrame;
    SurfaceHolder surfaceHolder;

    PictureCallback rawCallback;
    ShutterCallback shutterCallback;
    PictureCallback jpegCallback;
    AutoFocusCallback autoFocusCallback;
    boolean meteringAreaSupported;
    private static final int FOCUS_AREA_SIZE = 300;
    boolean takePicture;
    int surfaceViewWidth;
    int surfaceViewHeight;
    int cardFrameWidth;
    int cardFrameHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApp) getApplicationContext();
        activity = this;
        meteringAreaSupported = false;

//        ActionBar action_bar = getActionBar();

        setContentView(R.layout.activity_capture_card);

        // Adding the camera guide
        ImageView cameraGuide = (ImageView) findViewById(R.id.outside_imageview);
        Drawable pictGuide = getResources().getDrawable(R.drawable.ic_creditcard);

        // setting the opacity (alpha)
        pictGuide.setAlpha(80);

        // setting the images on the ImageViews
        cameraGuide.setImageDrawable(pictGuide);

        guidedFrame = (ImageView) findViewById(R.id.outside_imageview);

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    takePicture = false;
                    focusOnTouch(motionEvent);
                }
                return true;
            }
        });
        surfaceHolder = surfaceView.getHolder();
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder.addCallback(this);

        //camera = Camera.open();

        rawCallback = new PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
//                Log.d("Log", "onPictureTaken - raw");
            }
        };

        /** Handles data for jpeg picture */
        shutterCallback = new ShutterCallback() {
            public void onShutter() {
//                Log.i("Log", "onShutter'd");
            }
        };

        jpegCallback = new PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {

                try {
                    String filename = (myApp.frontView) ?
                            myApp.frontCardFilename : myApp.backCardFilename;
                    File file = new File(filename);
                    if (file.exists()) {
                        file.delete();
                    }

                    String thumb_filename = (myApp.frontView) ?
                            myApp.thumbFrontCardFilename : myApp.thumbBackCardFilename;
                    File thumb_file = new File(thumb_filename);
                    if (thumb_file.exists()) {
                        thumb_file.delete();
                    }

                    // Save original file
                    // TRIAL-ADDITION
                    FileOutputStream fos1 = new FileOutputStream(file);
                    fos1.write(data);
                    fos1.close();

                    // Save thumbnail file
                    int thumbnailSize = (int) getResources().getDimension(R.dimen.photo_thumbnail_height);
                    Bitmap bitmap = decodeFile(file, thumbnailSize, thumbnailSize);
//                    Bitmap thumb_bitmap = Bitmap.createBitmap(bitmap, 0, 0, 300, 100);

                    Bitmap thumb_bitmap = ThumbnailUtils.extractThumbnail(bitmap, thumbnailSize, thumbnailSize);
                    FileOutputStream fos2 = new FileOutputStream(thumb_file);
                    thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos2);
                    fos2.close();

                    setActivityResult(true);

                } catch (Exception e) {
                    myApp.logE("Fail to save card image", e);
                    setActivityResult(false);
                }
                finish();
            }
        };

        autoFocusCallback = new AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                //take the picture
                if (takePicture) {
                    camera.takePicture(null, null, jpegCallback);
                }
            }
        };

        Button buttonCapture = (Button) findViewById(R.id.button_capture);
        buttonCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage(view);
            }
        });
    }


    public void captureImage(View v) {

        try {
            if (camera != null) {

                takePicture = true;
                focusOnCenter();
            }

        } catch (Exception e) {

        }
    }

    public void refreshCamera() {

        if (surfaceHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_capture_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
//        if (itemId == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        camera.stopPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        Camera.Parameters params = camera.getParameters();
//        if (params.getMaxNumMeteringAreas() > 0) {
//            this.meteringAreaSupported = true;
//        }

        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
        Camera.Size selected = sizes.get(0);
        params.setPreviewSize(selected.width,selected.height);
//        params.setPreviewSize(w, h);
        camera.setParameters(params);

        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        refreshCamera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            // open the camera
            camera = Camera.open();
        } catch (RuntimeException e) {
            // check for exceptions
            System.err.println(e);
            return;
        }

        surfaceViewWidth = surfaceView.getWidth();
        surfaceViewHeight = surfaceView.getHeight();
        cardFrameHeight = guidedFrame.getMeasuredHeight();
        cardFrameWidth = guidedFrame.getMeasuredWidth();

        Camera.Parameters params = camera.getParameters();

        List<Camera.Size> previewSizes = params.getSupportedPreviewSizes();
        Camera.Size previewSize = previewSizes.get(0);

        params.setPreviewSize(previewSize.width, previewSize.height);
        params.setPictureFormat(ImageFormat.JPEG);
        camera.setParameters(params);

        try {
            // The Surface has been created, now tell the camera where to draw
            // the preview.
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
            return;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // stop preview and release camera
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    private void focusOnTouch(MotionEvent event) {
        try {
            if (camera != null) {
                Camera.Parameters parameters = camera.getParameters();
                if (parameters.getMaxNumFocusAreas() > 0) {
                    float x = event.getX();
                    float y = event.getY();
                    Rect rect = calculateFocusArea(x, y);
//                    String str = String.format("X:%f Y:%f", x, y);
//                    Toast.makeText(activity, str, Toast.LENGTH_LONG).show();

                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    ArrayList<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
                    focusAreas.add(new Camera.Area(rect, 1000));
                    parameters.setFocusAreas(focusAreas);

                    camera.setParameters(parameters);
                    camera.autoFocus(autoFocusCallback);
                } else {
                    camera.autoFocus(autoFocusCallback);
                }
            }
        } catch (Exception e) {
            String str = String.format("Exception : %s", e.getMessage());
//            Toast.makeText(activity, str, Toast.LENGTH_LONG).show();
        }
    }

    private void focusOnCenter() {
        try {
            if (camera != null) {
                camera.cancelAutoFocus();

                Camera.Parameters parameters = camera.getParameters();
                if (parameters.getMaxNumFocusAreas() > 0) {
                    float x = surfaceViewWidth / 2;
                    float y = surfaceViewHeight / 2;
                    Rect rect = calculateFocusArea(x, y);
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    ArrayList<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
                    focusAreas.add(new Camera.Area(rect, 1000));
                    parameters.setFocusAreas(focusAreas);
                    camera.setParameters(parameters);
                    camera.autoFocus(autoFocusCallback);
                } else {
                    camera.autoFocus(autoFocusCallback);
                }
            }
        } catch (Exception e) {
            String str = String.format("Exception : %s", e.getMessage());
        }
    }

    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / surfaceViewWidth) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / surfaceViewHeight) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper) + focusAreaSize / 2 > 1000) {
            if (touchCoordinateInCameraReper > 0) {
                result = 1000 - focusAreaSize / 2;
            } else {
                result = -1000 + focusAreaSize / 2;
            }
        } else {
            result = touchCoordinateInCameraReper - focusAreaSize / 2;
        }
        return result;
    }

    private void setActivityResult(boolean success) {
        Intent data = new Intent();
        data.putExtra("success", success);
        setResult(MyApp.REQ_CODE_CAPTURE, data);
    }

    private static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    @Override
    public void onShutter() {
        Toast.makeText(this, "Click!", Toast.LENGTH_SHORT).show();
    }
}