package com.dipwallet.dipwallet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.isseiaoki.simplecropview.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class CropCardImage extends ActionBarActivity {
    MyApp myApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myApp = (MyApp)getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_card_image);

        Uri uri = getIntent().getParcelableExtra("imageUri");
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        }catch (Exception e) { }

        final CropImageView cropImageView = (CropImageView)findViewById(R.id.cropImageView);

        // Set image for cropping
        cropImageView.setCropMode(CropImageView.CropMode.RATIO_FREE);
        cropImageView.setImageBitmap(bitmap);

        Button cropButton = (Button)findViewById(R.id.crop_button);

        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get cropped image, and show result.
                try {
                    String filename = (myApp.frontView) ?
                            myApp.frontCardFilename : myApp.backCardFilename;
                    File file = new File(filename);
                    if (file.exists()) {
                        file.delete();
                    }

                    String thumb_filename = (myApp.frontView) ?
                            myApp.thumbFrontCardFilename : myApp.thumbBackCardFilename;
                    File thumb_file = new File(thumb_filename);
                    if (thumb_file.exists()) {
                        thumb_file.delete();
                    }

                    // Save original file
                    // TRIAL-ADDITION
                    FileOutputStream fos1 = new FileOutputStream(file);
                    cropImageView.getCroppedBitmap().compress(Bitmap.CompressFormat.JPEG, 80, fos1);
                    fos1.close();

                    // Save thumbnail file
                    int thumbnailSize = (int) getResources().getDimension(R.dimen.photo_thumbnail_height);
                    Bitmap bitmap = decodeFile(file, thumbnailSize, thumbnailSize);

                    Bitmap thumb_bitmap = ThumbnailUtils.extractThumbnail(bitmap, thumbnailSize, thumbnailSize);
                    FileOutputStream fos2 = new FileOutputStream(thumb_file);
                    thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos2);
                    fos2.close();

                    setActivityResult(true);
                }catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Error in cropping image", Toast.LENGTH_SHORT).show();
                    setActivityResult(false);
                }
                finish();
            }
        });
    }
    private void setActivityResult(boolean success) {
        Intent data = new Intent();
        data.putExtra("crop", success);
        setResult(MyApp.PIC_CROP, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crop_card_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
