package com.dipwallet.dipwallet;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

/**
 * Created by Made Budiarta on 2/26/2015.
 */
public class MyLogDb extends SQLiteOpenHelper {

    private final String LOG_TAG = "MyLogDb";
    private String dbFilename;

    public MyLogDb(Context context, String db_filename, int db_version) {
        super(context, db_filename, null, db_version);
        dbFilename = db_filename;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {

    }

    private void logE(String msg, Exception e) {
        String error = (e.getMessage() == null) ? "" : e.getMessage();
        Log.e(LOG_TAG, String.format("%s: %s", msg, error));
    }

    private void createDb(SQLiteDatabase db) {
        try {
            // Create table system_log
            String sql = "CREATE TABLE IF NOT EXISTS system_log ( ";
            sql += "log_time BIGINT, ";
            sql += "log_type TEXT, ";
            sql += "log_text TEXT )";
            db.execSQL(sql);

        } catch (Exception e) {
            logE("Fail to create database", e);
        }
    }

    private SQLiteDatabase openDb() {
        try {
            return SQLiteDatabase.openDatabase(dbFilename, null,
                    SQLiteDatabase.OPEN_READWRITE);

        } catch (Exception e) {
            logE(String.format("Fail to open database '%s'", dbFilename), e);
            return null;
        }
    }

    public void addLog(Date log_time, int log_type, String log_text) {
        try {
            SQLiteDatabase db = openDb();
            ContentValues values = new ContentValues();
            values.put("log_time", log_time.getTime());
            values.put("log_type", log_type);
            values.put("log_text", log_text);
            db.beginTransaction();
            try {
                db.insert("system_log", "", values);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            db.close();

        } catch (Exception e) {
            logE("Fail to add log", e);
        }
    }

    public void deleteExpiredLogs(Date exp_date) {
        try {
            SQLiteDatabase db = openDb();
            String where = "log_time < " + String.valueOf(exp_date.getTime());
            db.delete("system_log", where, null);
            db.close();

        } catch (Exception e) {
            logE("Fail to delete expired logs", e);
        }
    }
}

