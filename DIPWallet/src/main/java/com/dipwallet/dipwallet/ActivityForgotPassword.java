package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityForgotPassword extends Activity {

    private Activity activity;
    private MyApp myApp;
    private EditText edit_resend_email;
    private Button button_cancel_resend;
    private Button button_resend;
    private AsyncTaskXMPPClient task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        myApp = (MyApp) getApplicationContext();

        ActionBar action_bar = getActionBar();
//        action_bar.hide();

        setContentView(R.layout.activity_forgot_password);

        edit_resend_email = (EditText) findViewById(R.id.edit_resend_email);

        button_cancel_resend = (Button) findViewById(R.id.button_cancel_resend);
        button_cancel_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Close me
                finish();
            }
        });

        button_resend = (Button) findViewById(R.id.button_resend);
        button_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickResend();
            }
        });

        // Test only
        edit_resend_email.setText("made@datindo.co.id");
        // End of test
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_forgot_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void onClickResend() {
        try {
            // Email
            String email = edit_resend_email.getText().toString();
            if (email.isEmpty()) {
                Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
                edit_resend_email.requestFocus();
            }
            // Signin
            myApp.resendEmail = email;
            task = new AsyncTaskXMPPClient(this, MyApp.TASK_ID_RESEND_PASSWORD);
            task.setOnTaskCompleteListener(new OnTaskCompleteListener() {
                @Override
                public void onTaskComplete(boolean result, String resultText) {
                    if (result) {
                        // Close me
                        finish();
                    }
                }
            });
            task.execute();

        } catch (Exception e) {
            myApp.logE("Fail on click resend", e);
        }
    }
}
