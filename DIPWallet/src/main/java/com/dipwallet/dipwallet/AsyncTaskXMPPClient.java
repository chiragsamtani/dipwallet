package com.dipwallet.dipwallet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import org.jivesoftware.smack.AbstractXMPPConnection;

/**
 * Created by Made Budiarta on 3/26/2015.
 */
public class AsyncTaskXMPPClient extends AsyncTask<Void, String, Boolean> {

    private Activity activity;
    private int taskId;
    private MyApp myApp;
    private ProgressDialog dialog;
    private OnTaskCompleteListener onTaskCompleteListener;
    private MyXMPPClient xmppClient;

    public AsyncTaskXMPPClient(Activity a, int task_id) {
        activity = a;
        taskId = task_id;
        myApp = (MyApp) activity.getApplicationContext();
        dialog = new ProgressDialog(activity);
        onTaskCompleteListener = null;
        xmppClient = new MyXMPPClient(myApp);
    }

    @Override
    protected void onPreExecute() {
        // Set title
        String title = "";
        String msg = "";
        switch (taskId) {
            case MyApp.TASK_ID_REGISTER: title = "Register"; break;
            case MyApp.TASK_ID_SIGNIN: title = "Signin"; break;
            case MyApp.TASK_ID_RESEND_PASSWORD: title = "Resend Password"; break;
            case MyApp.TASK_ID_READ_DATA: title = "Read Data"; break;
            case MyApp.TASK_ID_UPDATE_DATA: title = "Update Data"; break;
            case MyApp.TASK_ID_CHANGE_PWD: title = "Change Password"; break;
        }
        dialog.setTitle(title);
        dialog.setMessage("Please wait..");
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            if (taskId == MyApp.TASK_ID_REGISTER) {
                publishProgress("Registering..");
                return xmppClient.register();
            } else if (taskId == MyApp.TASK_ID_SIGNIN) {
                publishProgress("Authenticating..");
                return xmppClient.signin();
            } else if (taskId == MyApp.TASK_ID_RESEND_PASSWORD) {
                publishProgress("Resending..");
                return xmppClient.resendPassword();
            } else {
                xmppClient.resultText = "Unknown Task";
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
        myApp.logD(values[0]);
    }

    @Override
    protected void onPostExecute(final Boolean result) {

        try {
            final String resultText = xmppClient.resultText;
            if (!resultText.isEmpty()) {
                dialog.setMessage(resultText);
            }
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                    if (onTaskCompleteListener != null) {
                        onTaskCompleteListener.onTaskComplete(result, resultText);
                    }
                }
            }, 1000);
        } catch (Exception e) {
            myApp.logE("Fail on post execute of AsyncTaskXMPPClient", e);
        }
    }

    public void setOnTaskCompleteListener(OnTaskCompleteListener listener) {
        onTaskCompleteListener = listener;
    }
}
