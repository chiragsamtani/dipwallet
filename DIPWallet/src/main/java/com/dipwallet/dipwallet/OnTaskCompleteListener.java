package com.dipwallet.dipwallet;

/**
 * Created by Made Budiarta on 3/30/2015.
 */
public interface OnTaskCompleteListener {
    void onTaskComplete(boolean result, String resultText);
}
