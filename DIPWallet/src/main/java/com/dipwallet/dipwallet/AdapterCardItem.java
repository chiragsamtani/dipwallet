package com.dipwallet.dipwallet;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayInputStream;

/**
 * Created by Made Budiarta on 3/4/2015.
 */
public class AdapterCardItem extends CursorAdapter {

    private int COL_ID_CARD_ID = 1;
    private int COL_ID_PREVIEW_IMAGE = 2;
    private int COL_ID_CARD_TITLE = 3;
    private int COL_ID_CARD_NO = 4;
    private int COL_ID_CARDHOLDER_NAME = 5;
    private int COL_ID_CATEGORY = 6;

    private Activity activity;
    private String sql;
    private MyApp myApp;
    private LayoutInflater layoutInflater;


    public AdapterCardItem(Activity activity, Cursor cursor, String sql) {
        super(activity, cursor, 0);
        this.activity = activity;
        this.sql = sql;
        myApp = (MyApp) activity.getApplicationContext();
        layoutInflater = activity.getLayoutInflater();
    }

    private static class CardItemViewHolder {
        //        LinearLayout layout_card_item_header;
        TextView text_card_item_card_title;
        ImageButton button_edit_card;
        ImageButton button_delete_card;
        //        LinearLayout layout_card_item_content;
        ImageView image_card_item_preview_image;
        LinearLayout layout_card_item_data;
        TextView text_card_item_card_no;
        TextView text_card_item_cardholder_name;
        TextView text_card_item_category_name;
        LinearLayout background_adapter;
        ImageView adapter_icon_based_on_category;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor == null)
            return;

        try {
            int card_id = cursor.getInt(COL_ID_CARD_ID);

            String card_title = cursor.getString(COL_ID_CARD_TITLE);
            String cardholder_name = cursor.getString(COL_ID_CARDHOLDER_NAME);
            String card_no = cursor.getString(COL_ID_CARD_NO);

            String category = cursor.getString(COL_ID_CATEGORY);

            byte[] blob = cursor.getBlob(COL_ID_PREVIEW_IMAGE);
            ByteArrayInputStream stream = new ByteArrayInputStream(blob);
            Bitmap card_preview = BitmapFactory.decodeStream(stream);
            int height = (int) myApp.getResources().getDimension(R.dimen.photo_height);
            int width = (int) myApp.getResources().getDimension(R.dimen.photo_width);

            final CardItemViewHolder holder = (CardItemViewHolder) view.getTag();

            // Set card title
            holder.text_card_item_card_title.setText(card_title);
            // Set button edit card
            holder.button_edit_card.setTag(card_id);
            // Set button delete card
            holder.button_delete_card.setTag(card_id);

            // Set preview image
            holder.image_card_item_preview_image.setTag(card_id);
//            holder.image_card_item_preview_image.setImageBitmap(card_preview);
            holder.image_card_item_preview_image.setImageBitmap(card_preview);

            // Set layout card item data
            holder.layout_card_item_data.setTag(card_id);
            // Set card no
            holder.text_card_item_card_no.setText(card_no);
            // Set cardholder name
            holder.text_card_item_cardholder_name.setText(cardholder_name);

            // for color
            switch (category) {
                case "Fashion":
                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.PinkCardColor));
//                    int colors[] = {R.color.PinkCardColor, R.color.PinkCardGradient};
//                    GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.TR_BL, colors);
//                    holder.background_adapter.setBackground(g);
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.tee);
                    break;
                case "Food":
                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.GreenCardColor));
                    holder.image_card_item_preview_image.setBackgroundColor(myApp.getColor(R.color.GreenCardColor));
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.resto);
                    break;
                case "Medical":
                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.RedCardColor));
                    holder.image_card_item_preview_image.setBackgroundColor(myApp.getColor(R.color.RedCardColor));
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.gray_new_medical);
                    break;
                case "Hotels":
//                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.BlueCardColor));
//                    holder.image_card_item_preview_image.setBackgroundColor(myApp.getColor(R.color.BlueCardColor));
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.hotels);
                    break;
                case "Sports":
                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.OrangeCardColor));
                    holder.image_card_item_preview_image.setBackgroundColor(myApp.getColor(R.color.OrangeCardColor));
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.football);
                    break;
                case "Others":
                    holder.background_adapter.setBackgroundColor(myApp.getColor(R.color.YellowCardColor));
                    holder.image_card_item_preview_image.setBackgroundColor(myApp.getColor(R.color.YellowCardColor));
                    holder.adapter_icon_based_on_category.setImageResource(R.drawable.bulb);
                    break;
            }

            holder.text_card_item_category_name.setText(category);


        } catch (Exception e) {
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        if (cursor == null) {
            return null;
        } else {
            View view = layoutInflater.inflate(R.layout.adapter_card_versiontwo, null);
            CardItemViewHolder holder = new CardItemViewHolder();
//            holder.layout_card_item_header = (LinearLayout) view.findViewById(R.id.layout_card_item_header);
            holder.text_card_item_card_title = (TextView) view.findViewById(R.id.text_card_item_card_title);
            holder.button_edit_card = (ImageButton) view.findViewById(R.id.button_edit_card);
            holder.button_delete_card = (ImageButton) view.findViewById(R.id.button_delete_card);
//            holder.layout_card_item_content = (LinearLayout) view.findViewById(R.id.layout_card_item_content);
            holder.image_card_item_preview_image = (ImageView) view.findViewById(R.id.image_card_item_preview_image);
            holder.layout_card_item_data = (LinearLayout) view.findViewById(R.id.layout_card_item_data);
            holder.text_card_item_card_no = (TextView) view.findViewById(R.id.text_card_item_card_no);
            holder.text_card_item_cardholder_name = (TextView) view.findViewById(R.id.text_card_item_cardholder_name);

            holder.text_card_item_category_name = (TextView)view.findViewById(R.id.text_card_item_category_name);
            holder.background_adapter = (LinearLayout)view.findViewById(R.id.background_adapter);
//            holder.background_adapter = (RelativeLayout)view.findViewById(R.id.background_adapter);
            holder.adapter_icon_based_on_category = (ImageView)view.findViewById(R.id.text_card_item_card_icon);

            view.setTag(holder);
            return view;
        }
    }

    public void refresh() {
        try {
            Cursor cursor = myApp.myCardDb.getCardsCursor(sql);
            changeCursor(cursor);

        } catch (Exception e) {
            myApp.logE("Fail to refresh card item adapter", e);
        }
    }
}