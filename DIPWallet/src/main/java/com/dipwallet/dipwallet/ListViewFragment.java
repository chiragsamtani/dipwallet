package com.dipwallet.dipwallet;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chandra on 8/24/15.
 */
public class ListViewFragment extends Fragment {

    public final static String ITEMS_COUNT_KEY = "PartThreeFragment$ItemsCount";
    public final static String SQL_STRING = "FragmentSql$String";
    private static Cursor mCursor;
    private static String mSql;
    private Activity mActivity;
    private MyApp myApp;

    public MyListCursorAdapter recyclerAdapter;
    public RecyclerView recyclerView;
//
//    public static ListViewFragment createInstance(int itemsCount) {
////        // Making the list
////        ListViewFragment partThreeFragment = new ListViewFragment();
////        Bundle bundle = new Bundle();
////        bundle.putInt(ITEMS_COUNT_KEY, itemsCount);
////        partThreeFragment.setArguments(bundle);
////        return partThreeFragment;
//    }

//    public static ListViewFragment createInstance(Cursor cursor, String sql) {
//        ListViewFragment fragment = new ListViewFragment();
//        mCursor = cursor;
//        mSql = sql;
//
//        return fragment;
//    }

    public static ListViewFragment createInstance(String sql) {
        ListViewFragment fragment = new ListViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SQL_STRING, sql);
        fragment.setArguments(bundle);
//        mCursor = cursor;
//        mSql = sql;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myApp = (MyApp) getActivity().getApplicationContext();

        // Fragment layout
        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.trial_fragment, container, false);
//        ListView listView = (ListView) inflater.inflate(R.layout.trial_fragment, container, false);
//
        // setup the inside of the Recyclerview
        setupRecyclerView(recyclerView);
        return recyclerView;

//        // setup the inside of the Listview
//        setupListView(listView);
//        return listView;
    }
//
    public void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // calling adapter
//        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(createItemList());
//        recyclerView.setAdapter(recyclerAdapter);

        // calling adapter
        this.mActivity = getActivity();
        recyclerAdapter = new MyListCursorAdapter(mActivity.getApplicationContext(), retrieveCardsByCategory());
        recyclerView.setAdapter(recyclerAdapter);
    }

//    private List<String> createItemList() {
//
//        // list of items
//        List<String> itemList = new ArrayList<>();
//        Bundle bundle = getArguments();
//        if(bundle!=null) {
//            int itemsCount = bundle.getInt(ITEMS_COUNT_KEY);
//            for (int i = 0; i < itemsCount; i++) {
//                itemList.add("Item " + i);
//            }
//        }
//        return itemList;
//    }

    private Cursor retrieveCardsByCategory(){
        mCursor = null;
        Bundle bundle = getArguments();
        String sql = bundle.getString(SQL_STRING);

        if (!sql.isEmpty()) {
            mCursor = myApp.myCardDb.getCardsCursor(sql);
//            if (cursor != null) {
//                adapterCardItem = new AdapterCardItem(this, cursor, sql);
//                listCardItem.setAdapter(adapterCardItem);
//                adapterCardItem.refresh();
//            }

        }
        return mCursor;
    }
}