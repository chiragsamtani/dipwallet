package com.dipwallet.dipwallet;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Made Budiarta on 3/3/2015.
 */
public class AdapterCardTitle extends ArrayAdapter<String> implements Filterable {

    private int layoutRecourceId;
    private Activity activity;
    private MyApp myApp;
    private ArrayList<String> cardTitles;
    private ArrayList<String> allCardTitles;
    private Filter filter;

    public AdapterCardTitle(Context context, int layoutResourceId) {
        super(context, layoutResourceId);
        this.layoutRecourceId = layoutResourceId;
        activity = (Activity) context;
        myApp = (MyApp) activity.getApplicationContext();
        cardTitles = myApp.cardTitles;
        allCardTitles = myApp.cardTitles;
    }

    @Override
    public int getCount() {
        return cardTitles.size();
    }

    @Override
    public String getItem(int position) {
        return cardTitles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class CardTitleHolder {
        TextView text_card_title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try {
            CardTitleHolder holder = null;
            final String card_title = cardTitles.get(position);

            if (convertView == null) {
                LayoutInflater inflater = activity.getLayoutInflater();
                convertView = inflater.inflate(layoutRecourceId, null);
                holder = new CardTitleHolder();
                holder.text_card_title = (TextView) convertView.findViewById(R.id.text_card_title);
                convertView.setTag(holder);
            } else {
                holder = (CardTitleHolder) convertView.getTag();
            }
            holder.text_card_title.setText(card_title);
            return convertView;

        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CardTitleFilter();
        }
        return filter;
    }

    private class CardTitleFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if ((charSequence == null)||(charSequence.length()==0)) {
                cardTitles = allCardTitles;
                notifyDataSetChanged();

                results.values = cardTitles;
                results.count = cardTitles.size();
            } else {
                ArrayList<String> filtered_card_titles = new ArrayList<String>();
                for (String card_title : allCardTitles) {
                    String str = card_title.toLowerCase();
                    String filter_str = charSequence.toString().toLowerCase();
                    if (str.contains(filter_str)) {
                        filtered_card_titles.add(card_title);
                    }
                    results.values = filtered_card_titles;
                    results.count = filtered_card_titles.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            if (filterResults.count == 0) {
                notifyDataSetInvalidated();
            } else {
                cardTitles = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        }
    }
}
