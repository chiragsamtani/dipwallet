package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dipwallet.dipwallet.ActivityViewCardImage;
import com.dipwallet.dipwallet.MyApp;
import com.dipwallet.dipwallet.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code39Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.encoder.QRCode;

public class ActivityViewCard extends Activity {

    private static final int IMAGE_TYPE_FRONT = 0;
    private static final int IMAGE_TYPE_BACK = 1;
    private static final int BARCODE_FORMAT_CODE39 = 0;
    private static final int BARCODE_FORMAT_QRCODE = 1;

    private MyApp myApp;
    private ImageView imageViewCardImage;
    private ImageButton buttonViewFront;
    private ImageButton buttonViewBack;
    private ImageView imageViewCardBarCode;
    private ImageButton buttonViewBarCode;
    private ImageButton buttonViewQRCode;
    private TextView textViewCardTitle;
    private TextView textViewCardNo;
    private TextView textViewCardName;
    private TextView textViewCardNote;
    private MyApp.CardData cardData;
    private int imageType;
    private int barcodeFormat;
    private int previewBarCodeWidth;
    private int previewBarCodeHeight;
    private int previewQRCodeWidth;
    private int previewQRCodeHeight;
    private int fullBarCodeWidth;
    private int fullBarCodeHeight;
    private int fullQRCodeWidth;
    private int fullQRCodeHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApp) getApplicationContext();
        cardData = myApp.cardData;
        previewBarCodeWidth = myApp.displayWidth - 40;
        previewBarCodeHeight = 200;
        fullBarCodeWidth = myApp.displayHeight - 40;
        fullBarCodeHeight = 200;
        previewQRCodeWidth = myApp.displayWidth - 40;
        previewQRCodeHeight = myApp.displayWidth - 40;
        fullQRCodeWidth = myApp.displayWidth - 40;
        fullQRCodeHeight = myApp.displayWidth - 40;

        ActionBar action_bar = getActionBar();
       // action_bar.hide();

        setContentView(R.layout.activity_view_card);

        imageViewCardImage = (ImageView) findViewById(R.id.image_view_card_image);
        imageViewCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFullCardImage();
            }
        });

        buttonViewFront = (ImageButton) findViewById(R.id.button_view_front);
        buttonViewFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageType = IMAGE_TYPE_FRONT;
                showPreviewCardImage();
            }
        });

        buttonViewBack = (ImageButton) findViewById(R.id.button_view_back);
        buttonViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageType = IMAGE_TYPE_BACK;
                showPreviewCardImage();
            }
        });

        imageViewCardBarCode = (ImageView) findViewById(R.id.image_view_card_barcode);
        imageViewCardBarCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFullBarCodeImage();
            }
        });

        buttonViewBarCode = (ImageButton) findViewById(R.id.button_view_barcode);
        buttonViewBarCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodeFormat = BARCODE_FORMAT_CODE39;
                showPreviewBarCodeImage();
            }
        });

        buttonViewQRCode = (ImageButton) findViewById(R.id.button_view_qrcode);
        buttonViewQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodeFormat = BARCODE_FORMAT_QRCODE;
                showPreviewBarCodeImage();
            }
        });

        textViewCardTitle = (TextView) findViewById(R.id.text_view_card_title);
        textViewCardNo = (TextView) findViewById(R.id.text_view_card_no);
        textViewCardName = (TextView) findViewById(R.id.text_view_card_name);
        textViewCardNote = (TextView) findViewById(R.id.text_view_card_note);

        showCard();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_activity_view_card, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        return super.onOptionsItemSelected(item);
//    }

    private void showCard() {
        try {
            imageType = IMAGE_TYPE_FRONT;
            showPreviewCardImage();
            barcodeFormat = BARCODE_FORMAT_CODE39;
            showPreviewBarCodeImage();
            textViewCardTitle.setText(cardData.cardTitle);
            textViewCardNo.setText(cardData.cardNo);
            textViewCardName.setText(cardData.cardholderName);
            if (cardData.cardNote.isEmpty()) {
                textViewCardNote.setVisibility(View.GONE);
                textViewCardNote.setText("");
            } else {
                textViewCardNote.setVisibility(View.VISIBLE);
                textViewCardNote.setText(cardData.cardNote);
            }

        } catch (Exception e) {
            myApp.logE("Fail to show card", e);
        }
    }

    private Bitmap generateBarCodeBitmap(int width, int height) {
        try {
            Bitmap bitmap = null;
            Code39Writer writer = new Code39Writer();
            BitMatrix bitMatrix =  writer.encode(cardData.cardNo, BarcodeFormat.CODE_39, width, height);
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, bitMatrix.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            return bitmap;

        } catch (Exception e) {
            myApp.logE("Fail to generate bar code bitmap", e);
            return null;
        }
    }

    private Bitmap generateQRCodeBitmap(int width, int height) {
        try {
            Bitmap bitmap = null;
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix =  qrCodeWriter.encode(cardData.cardNo, BarcodeFormat.QR_CODE, width, height);
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    bitmap.setPixel(i, j, bitMatrix.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            return bitmap;

        } catch (Exception e) {
            myApp.logE("Fail to generate qr code bitmap", e);
            return null;
        }
    }

    private void showFullCardImage() {
        try {
            if (imageType == IMAGE_TYPE_FRONT) {
                myApp.cardImage = cardData.frontImage;
            } else if (imageType == IMAGE_TYPE_BACK) {
                myApp.cardImage = cardData.backImage;
            }
            if (myApp.cardImage != null) {
                Intent intent = new Intent(this, ActivityViewCardImage.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            myApp.logE("Fail to show full card image", e);
        }
    }

    private void showFullBarCodeImage() {
        try {
            if (barcodeFormat == BARCODE_FORMAT_CODE39) {
                myApp.cardImage = generateBarCodeBitmap(fullBarCodeWidth, fullBarCodeHeight);
            } else if (barcodeFormat == BARCODE_FORMAT_QRCODE) {
                myApp.cardImage = generateQRCodeBitmap(fullQRCodeWidth, fullQRCodeHeight);
            }
            if (myApp.cardImage != null) {
                Intent intent = new Intent(this, ActivityViewCardImage.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            myApp.logE("Fail to show full bar code image", e);
        }
    }

    private void showPreviewCardImage() {
        if (imageType == IMAGE_TYPE_FRONT) {
            imageViewCardImage.setImageBitmap(cardData.frontImage);
        } else if (imageType == IMAGE_TYPE_BACK) {
            imageViewCardImage.setImageBitmap(cardData.backImage);
        }
    }

    private void showPreviewBarCodeImage() {
        if (barcodeFormat == BARCODE_FORMAT_CODE39) {
            Bitmap bitmap = generateBarCodeBitmap(previewBarCodeWidth, previewBarCodeHeight);
            imageViewCardBarCode.setImageBitmap(bitmap);

        } else if (barcodeFormat == BARCODE_FORMAT_QRCODE) {
            Bitmap bitmap = generateQRCodeBitmap(previewQRCodeWidth, previewQRCodeHeight);
            imageViewCardBarCode.setImageBitmap(bitmap);
        }
    }
}
