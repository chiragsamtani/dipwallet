package com.dipwallet.dipwallet;

import android.content.Context;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.SSLContext;

/**
 * Created by Made Budiarta on 3/26/2015.
 */

public class MyXMPPClient {

    private MyApp myApp;
    private AbstractXMPPConnection connection;
    private int waitTimeout;
    private boolean receivedResponse;
    private String receivedMessage;
    private String clientUserId;
    private String clientPassword;
    private String adminEmail;
    private JSONObject resultJSON;
    private String responseCode;
    public String resultText;

    public MyXMPPClient(Context context) {
        myApp = (MyApp) context;
        waitTimeout = 3;
        receivedResponse = false;
        receivedMessage = "";
        clientUserId = "datindo.co.id.clientxmpp";
        clientPassword = "dip12345678";
        adminEmail = "datindo.co.id.adminxmpp@datindo.co.id";
        responseCode = "";
        resultText = "";

        try {
//            // Prepare connection
//            connection = prepareConnection();

        } catch (Exception e) {
            logE("Fail on MyXMPPClient construction", e);
        }
    }

    private void logE(String msg, Exception e) {
        resultText = msg;
        myApp.logE(msg, e);
    }

    private AbstractXMPPConnection prepareConnection() {
        try {
            AbstractXMPPConnection connection = null;
            XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration.builder();
            builder.setHost("drc.datindo.co.id");
            builder.setPort(3311);
            builder.setServiceName("datindo.co.id");
            //builder.setDebuggerEnabled(true);
            builder.setResource(myApp.deviceId);
            try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, MemorizingTrustManager.getInstanceList(myApp), new SecureRandom());
                builder.setCustomSSLContext(sc);
            } catch (Exception e) {
                myApp.logE("Fail to init SSL", e);
            }
            connection = new XMPPTCPConnection(builder.build());
            PacketListener listener = new PacketListener() {
                @Override
                public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                    myApp.logD("Receive message..");
                    Message msg = (Message) packet;
                    receivedMessage = msg.getBody();
                    if ((receivedMessage != null)&&(!receivedMessage.isEmpty())) {
                        receivedResponse = true;
                        myApp.logD(String.format("Received: %s", receivedMessage));
                    }
                }
            };
            PacketFilter filter = MessageTypeFilter.CHAT;
            connection.addPacketListener(listener, filter);
            return connection;

        } catch (Exception e) {
            logE("Fail to prepare connection", e);
            return null;
        }
    }

    public boolean connect() {
        try {
            myApp.logD("Connect");
            if (connection.isConnected()) {
                return true;
            }
            connection.connect();
            return connection.isConnected();

        } catch (Exception e) {
            logE("Fail to connect", e);
            return false;
        }
    }

    public void disconnect() {
        try {
            myApp.logD("Disconnect");
            if (connection.isConnected()) {
                connection.disconnect();
            }
            Thread.sleep(500);

        } catch (Exception e) {
            logE("Fail to disconnect", e);
        }
    }

    public boolean login(String userName, String userPassword) {
        try {
            myApp.logD(String.format("Login %s/%s", userName, userPassword));
            connection.login(userName, userPassword);
            return connection.isAuthenticated();

        } catch (Exception e) {
            logE("Fail to login", e);
            return false;
        }
    }

    public boolean getResponse() {
        try {
            responseCode = "";
            resultText = "";

            long curTime = new Date().getTime();
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(curTime);
            cal.add(Calendar.SECOND, waitTimeout);
            long expTime = cal.getTime().getTime();
            while (curTime < expTime) {
                if (receivedResponse) {
                    JSONObject responseJSON = new JSONObject(receivedMessage);
                    resultJSON = responseJSON.getJSONObject("result");
                    if (resultJSON != null) {
                        responseCode = resultJSON.getString("code");
                        resultText = resultJSON.getString("message");
                        return true;
                    }
                }
                Thread.sleep(500);
                curTime = new Date().getTime();
            }
            resultText = "Timeout waiting response.";
            return false;

        } catch (Exception e) {
            myApp.logE("Fail to get response", e);
            return false;
        }
    }

    public boolean register() {
        try {
            myApp.logD("Register");
            // Prepare connection
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (login(clientUserId, clientPassword)) {
                    // Send message
                    JSONObject params = new JSONObject();
                    params.put("version", "1");
                    params.put("name", myApp.myName);
                    params.put("email", myApp.myEmail);
                    params.put("no_hp", myApp.myMobilePhone);
                    params.put("gender", myApp.myGender);
                    params.put("birth_day", myApp.myBirthday);
                    params.put("address", myApp.myAddress);
                    params.put("city", myApp.myCity);
                    params.put("imei", myApp.deviceId);
                    JSONObject sendJSON = new JSONObject();
                    sendJSON.put("jsonrpc", "2.0");
                    sendJSON.put("method", "register");
                    sendJSON.put("id", "1");
                    sendJSON.put("params", params);
                    String jsonStr = sendJSON.toString();
                    Message msg = new Message(adminEmail, Message.Type.chat);
                    msg.setBody(jsonStr);
                    myApp.logD(String.format("Send: %s", jsonStr));
                    connection.sendPacket(msg);
                    // Get response
                    if (getResponse()) {
                        if (responseCode.contentEquals("0")) {
                            success = true;
                        }
                    }
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to register", e);
            return false;
        }
    }

    public boolean signin() {
        try {
            myApp.logD("Signin");
            // Prepare connection
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (login(myApp.myUserId, myApp.myPassword)) {
                    success = true;
                    resultText = "Success";
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to signin", e);
            return false;
        }
    }

    public boolean changePassword(String newPassword) {
        try {
            myApp.logD("Change Password");
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (login(myApp.myUserId, myApp.myPassword)) {
                    // Send message
                    JSONObject params = new JSONObject();
                    params.put("user_id", myApp.myUserId);
                    params.put("password", newPassword);
                    params.put("old_password", myApp.myPassword);
                    JSONObject sendJSON = new JSONObject();
                    sendJSON.put("jsonrpc", "2.0");
                    sendJSON.put("method", "change_pwd");
                    sendJSON.put("id", "1");
                    sendJSON.put("params", params);
                    String jsonStr = sendJSON.toString();
                    Message msg = new Message(adminEmail, Message.Type.chat);
                    msg.setBody(jsonStr);
                    myApp.logD(String.format("Send: %s", jsonStr));
                    connection.sendPacket(msg);
                    // Get response
                    if (getResponse()) {
                        if (responseCode.contentEquals("0")) {
                            myApp.myPassword = newPassword;
                            success = true;
                        }
                    }
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to change password", e);
            return false;
        }
    }

    public boolean resendPassword() {
        try {
            myApp.logD("Resend Password");
            // Prepare connection
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (login(clientUserId, clientPassword)) {
                    // Send message
                    JSONObject params = new JSONObject();
                    params.put("email", myApp.resendEmail);
                    JSONObject sendJSON = new JSONObject();
                    sendJSON.put("jsonrpc", "2.0");
                    sendJSON.put("method", "resend_id_pwd");
                    sendJSON.put("id", "1");
                    sendJSON.put("params", params);
                    String jsonStr = sendJSON.toString();
                    Message msg = new Message(adminEmail, Message.Type.chat);
                    msg.setBody(jsonStr);
                    myApp.logD(String.format("Send: %s", jsonStr));
                    connection.sendPacket(msg);
                    // Get Response
                    success = getResponse();
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to resend password", e);
            return false;
        }
    }

    public boolean readData() {
        try {
            myApp.logD("Read Data");
            // Prepare connection
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (!login(clientUserId, clientPassword)) {
                    // Send message
                    JSONObject params = new JSONObject();
                    params.put("user_id", myApp.myUserId);
                    JSONObject sendJSON = new JSONObject();
                    sendJSON.put("jsonrpc", "2.0");
                    sendJSON.put("method", "read");
                    sendJSON.put("id", "1");
                    sendJSON.put("params", params);
                    String jsonStr = sendJSON.toString();
                    Message msg = new Message(adminEmail, Message.Type.chat);
                    msg.setBody(jsonStr);
                    myApp.logD(String.format("Send: %s", jsonStr));
                    connection.sendPacket(msg);
                    // Get Response
                    if (getResponse()) {
                        if (responseCode.contentEquals("0")) {
                            if (resultJSON.has("name")) {
                                myApp.myName = resultJSON.getString("name");
                            }
                            if (resultJSON.has("email")) {
                                myApp.myEmail = resultJSON.getString("email");
                            }
                            if (resultJSON.has("no_hp")) {
                                myApp.myMobilePhone = resultJSON.getString("no_hp");
                            }
                            if (resultJSON.has("gender")) {
                                myApp.myGender = resultJSON.getString("gender");
                            }
                            if (resultJSON.has("birth_day")) {
                                myApp.myBirthday = resultJSON.getString("birth_day");
                            }
                            if (resultJSON.has("address")) {
                                myApp.myAddress = resultJSON.getString("address");
                            }
                            if (resultJSON.has("city")) {
                                myApp.myCity = resultJSON.getString("city");
                            }
                            success = true;
                        }
                    }
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to read data", e);
            return false;
        }
    }

    public boolean updateData() {
        try {
            myApp.logD("Update Data");
            // Prepare connection
            connection = prepareConnection();
            if (connection == null) {
                return false;
            }
            boolean success = false;
            if (connect()) {
                if (!login(clientUserId, clientPassword)) {
                    // Send message
                    // Get Response

                    //Feature not implemented yet

                    success = getResponse();
                }
            }
            // Disconnect
            disconnect();
            return success;

        } catch (Exception e) {
            logE("Fail to update data", e);
            return false;
        }
    }
}
