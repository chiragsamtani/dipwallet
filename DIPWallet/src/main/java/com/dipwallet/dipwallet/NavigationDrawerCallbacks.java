package com.dipwallet.dipwallet;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
