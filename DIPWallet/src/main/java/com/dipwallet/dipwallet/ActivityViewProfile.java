package com.dipwallet.dipwallet;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class ActivityViewProfile extends ActionBarActivity {

    private TextView dataName;
    private MyApp myApp;
    private CircularImageView avatar;

    private EditText editName;
    private EditText editEmail;
    private EditText editPhone;
    private Bitmap initializerBitmap;
    private int thumbnailHeight;
    private int thumbnailWidth;
    private Button saveData;


    private Uri fileUri;
    private final int CAMERA_CODE = 0;
    private final int GALLERY_CODE = 1;
    final CharSequence[] items = {"Take Picture", "Choose from Gallery"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        myApp = (MyApp) getApplicationContext();

        thumbnailHeight = (int) getResources().getDimension(R.dimen.avatar_thumbnail_height);
        thumbnailWidth = (int) getResources().getDimension(R.dimen.avatar_thumbnail_width);
        dataName = (TextView) findViewById(R.id.txtUsername);
        avatar = (CircularImageView) findViewById(R.id.imgAvatar);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDialog();
            }
        });

        dataName.setText(myApp.myName);


        editName = (EditText) findViewById(R.id.edit_data_name);
        editEmail = (EditText) findViewById(R.id.edit_data_email);
        editPhone = (EditText) findViewById(R.id.edit_data_phone);


        //Initializing the View Profile page
        //to retain views already stored in the XMPP Clients

        editName.setText(myApp.myName);
        editEmail.setText(myApp.myEmail);
        editPhone.setText(myApp.myMobilePhone);

        //taking Bitmap from myApp, checking if null is necessary to avoid NullPointerException
        //creating a file inefficient??
        String tempFileName = myApp.avatarFileName;
        File file = new File(tempFileName);
        // if it exists then put the original avatar picture in the view profile circular view
        if (file.exists()) {
            initializerBitmap = decodeFile(file, thumbnailWidth, thumbnailHeight);
            avatar.setImageBitmap(initializerBitmap);
        } else {
            //otherwise set it equal to the template avatar and show that
            //in the view profile circular view
            avatar.setImageResource(R.drawable.new_avatar);
        }
        saveData = (Button) findViewById(R.id.saveDataButton);
        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveInfo();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_view_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Picture")) {
                    takePictureFromCamera();
                } else if (items[i].equals("Choose from Gallery")) {
                    takePictureFromGallery();

                } else {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //user selected take from camera
            //take picture from URI for higher quality and pass it on to a stream
            //saving of files is not done here yet!
            if (requestCode == CAMERA_CODE) {
                try {
                    Bitmap cameraPic = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
                    Bitmap rotateBitmap = null;

                    String[] projection = { MediaStore.Images.Media.DATA };
                    CursorLoader loader = new CursorLoader(this, fileUri, projection, null, null, null);
                    Cursor cursor = loader.loadInBackground();

                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();

                    // Rotation is stored in an EXIF tag, and this tag seems to return 0 for URIs.
                    // Hence, we retrieve it using an absolute path instead!
                    int rotation = 0;
                    String realPath = cursor.getString(column_index_data);
                    if (realPath != null) {
                        rotation = getRotationForImage(realPath);
                    }
                    ExifInterface orientationFixer = new ExifInterface(realPath);
                    if(rotation > 2) {
                        switch (rotation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotateBitmap = fixOrientation(cameraPic, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotateBitmap = fixOrientation(cameraPic, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotateBitmap = fixOrientation(cameraPic, 270);
                                break;
                            default:
                                break;
                        }
                    }else{
                        rotateBitmap = cameraPic;
                    }
                    String fileName = myApp.avatarFileName;
                    File file = new File(fileName);
                    if(file.exists()){
                        file.delete();
                    }
                    Bitmap thumbnail_pic = ThumbnailUtils.extractThumbnail(rotateBitmap, thumbnailWidth, thumbnailHeight);
                    if(thumbnail_pic != null) {
                        FileOutputStream fos = new FileOutputStream(file);
                        thumbnail_pic.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
                    }

                    avatar.setImageBitmap(thumbnail_pic);
                }catch (Exception e){

                }
            }
            //user took a picture from the gallery
            //extract it's cursor position
            else if (requestCode == GALLERY_CODE) {
                try {
                    System.out.println("Process of changing it to new URI via GALLERY");
                    Uri galleryUri = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = managedQuery(galleryUri, filePathColumn, null, null, null);
                    int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();
                    String picturePath = cursor.getString(columnIndex);

                    Bitmap galleryPic;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, options);
                    //the bigger the REQUIRED SIZE the more zoomed out the image will be
                    //can be reset to higher value if image is covering parts of original image

                    //scale always needs to remain 1
                    final int REQUIRED_SIZE = 500;
                    int scale = 1;
                    while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                        scale *= 2;
                    options.inSampleSize = scale;
                    options.inScaled = true;
                    options.inJustDecodeBounds = false;
                    String fileName = myApp.avatarFileName;
                    File file = new File(fileName);
                    if (file.exists()) {
                        file.delete();
                    }
                    File tempFile = new File(picturePath);
                    galleryPic = decodeFile(tempFile, thumbnailWidth, thumbnailHeight);
                    Bitmap thumbnail = ThumbnailUtils.extractThumbnail(galleryPic, thumbnailWidth, thumbnailHeight);
                    avatar.setImageBitmap(thumbnail);
                    if (thumbnail != null) {
                        FileOutputStream fos = new FileOutputStream(file);
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();

                    }

                } catch (Exception e) {

                }
            }
        } else {
            finish();
        }
    }

    public void saveInfo() {
        boolean result;
        try {
            if (!myApp.myName.equals(editName.getText().toString()))
                myApp.myName = editName.getText().toString();
            if (!myApp.myEmail.equals(editEmail.getText().toString()))
                myApp.myEmail = editEmail.getText().toString();
            if (!myApp.myMobilePhone.equals(editEmail.getText().toString()))
                myApp.myMobilePhone = editPhone.getText().toString();

            String filename = myApp.avatarFileName;
            File file = new File(filename);
            Bitmap avatar = null;
            if (file.exists()) {
                avatar = decodeFile(file, thumbnailWidth, thumbnailHeight);
                myApp.avatarImage = avatar;
            }
            result = true;
        } catch (Exception e) {
            System.out.println("Error on Updating profile");
            result = false;
        }
        setActivityResult(result);
        finish();
    }

    private void setActivityResult(boolean success) {
        Intent data = new Intent();
        data.putExtra("success", success);
        setResult(MyApp.TASK_UPDATE_PROFILE, data);
    }

    private void takePictureFromCamera() {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, UUID.randomUUID().toString() + ".jpg");
            fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(cameraIntent, CAMERA_CODE);

        } catch (Exception e) {
        }
    }

    private void takePictureFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_CODE);
    }

    private Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public Bitmap fixOrientation(Bitmap bitmap, int angle) {
        Bitmap nBitmap = bitmap;
        if (nBitmap.getWidth() > nBitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            nBitmap = Bitmap.createBitmap(nBitmap, 0, 0, nBitmap.getWidth(), nBitmap.getHeight(), matrix, true);
            return nBitmap;

        }
        return nBitmap;
    }

    public int getRotationForImage(String path) {
        int rotation = 0;

        try {
            ExifInterface exif = new ExifInterface(path);
            rotation = (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rotation;
    }
}
