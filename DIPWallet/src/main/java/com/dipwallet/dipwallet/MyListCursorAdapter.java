package com.dipwallet.dipwallet;

/**
 * Created by Chandra on 8/25/15.
 */
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.util.List;

/**
 * Created by skyfishjy on 10/31/14.
 */
public class MyListCursorAdapter extends CursorRecyclerViewAdapter<MyListCursorAdapter.ViewHolder>{

    private int COL_ID_CARD_ID = 1;
    private int COL_ID_PREVIEW_IMAGE = 2;
    private int COL_ID_CARD_TITLE = 3;
    private int COL_ID_CARD_NO = 4;
    private int COL_ID_CARDHOLDER_NAME = 5;
    private int COL_ID_CATEGORY = 6;

    private MyApp myApp;
    private String sql;


    public MyListCursorAdapter(Context context,Cursor cursor){
        super(context,cursor);
        myApp = (MyApp) context;

        if (cursor != null) {
            sql = cursor.toString();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //        LinearLayout layout_card_item_header;
        TextView text_card_item_card_title;
        ImageButton button_edit_card;
        ImageButton button_delete_card;
        //        LinearLayout layout_card_item_content;
        ImageView image_card_item_preview_image;
        LinearLayout layout_card_item_data;
        TextView text_card_item_card_no;
        TextView text_card_item_cardholder_name;
        TextView text_card_item_category_name;
        LinearLayout background_adapter;
        ImageView adapter_icon_based_on_category;

        public ViewHolder(View view) {
            super(view);
//            CardItemViewHolder holder = new CardItemViewHolder();

//            holder.layout_card_item_header = (LinearLayout) view.findViewById(R.id.layout_card_item_header);
            text_card_item_card_title = (TextView) view.findViewById(R.id.text_card_item_card_title);
            button_edit_card = (ImageButton) view.findViewById(R.id.button_edit_card);
            button_delete_card = (ImageButton) view.findViewById(R.id.button_delete_card);
//            holder.layout_card_item_content = (LinearLayout) view.findViewById(R.id.layout_card_item_content);
            image_card_item_preview_image = (ImageView) view.findViewById(R.id.image_card_item_preview_image);
            layout_card_item_data = (LinearLayout) view.findViewById(R.id.layout_card_item_data);
            text_card_item_card_no = (TextView) view.findViewById(R.id.text_card_item_card_no);
            text_card_item_cardholder_name = (TextView) view.findViewById(R.id.text_card_item_cardholder_name);

            text_card_item_category_name = (TextView)view.findViewById(R.id.text_card_item_category_name);
            background_adapter = (LinearLayout)view.findViewById(R.id.background_adapter);
//            holder.background_adapter = (RelativeLayout)view.findViewById(R.id.background_adapter);
            adapter_icon_based_on_category = (ImageView)view.findViewById(R.id.text_card_item_card_icon);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_card_v4, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        if (cursor == null)
            return;

        try {
            // get information from the cursor
            int card_id = cursor.getInt(COL_ID_CARD_ID);
            String card_title = cursor.getString(COL_ID_CARD_TITLE);
            String cardholder_name = cursor.getString(COL_ID_CARDHOLDER_NAME);
            String card_no = cursor.getString(COL_ID_CARD_NO);
            String category = cursor.getString(COL_ID_CATEGORY);
            byte[] blob = cursor.getBlob(COL_ID_PREVIEW_IMAGE);

            ByteArrayInputStream stream = new ByteArrayInputStream(blob);
            Bitmap card_preview = BitmapFactory.decodeStream(stream);
            int height = (int) myApp.getResources().getDimension(R.dimen.photo_height);
            int width = (int) myApp.getResources().getDimension(R.dimen.photo_width);

//            final CardItemViewHolder holder = (CardItemViewHolder) view.getTag();

            // Set card title
            holder.text_card_item_card_title.setText(card_title);
            // Set button edit card
            holder.button_edit_card.setTag(card_id);
            // Set button delete card
            holder.button_delete_card.setTag(card_id);

            // Set layout card item data
            holder.layout_card_item_data.setTag(card_id);
            // Set card no
            holder.text_card_item_card_no.setText(card_no);
            // Set cardholder name
            holder.text_card_item_cardholder_name.setText(cardholder_name);

            // Set image preview
            holder.image_card_item_preview_image.setImageBitmap(card_preview);
            // Set category
            holder.text_card_item_category_name.setText(category);




        } catch (RuntimeException e) {
            System.out.println(e.getClass().getName());
        }
//
//
//        MyListItem myListItem = MyListItem.fromCursor(cursor);
//        viewHolder.mTextView.setText(myListItem.getName());
    }

    public void refresh() {
        try {
            Cursor cursor = myApp.myCardDb.getCardsCursor(sql);
            swapCursor(cursor);

        } catch (Exception e) {
            myApp.logE("Fail to refresh card item adapter", e);
        }
    }
}