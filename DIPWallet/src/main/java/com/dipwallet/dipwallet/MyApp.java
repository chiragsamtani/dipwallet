package com.dipwallet.dipwallet;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Made Budiarta on 2/5/2015.
 */
public class MyApp extends Application {

    public static final String LOG_TAG = "MyApp";
    // Log Type
    public static final int LOG_TYPE_DEBUG = 0;
    public static final int LOG_TYPE_INFO = 1;
    public static final int LOG_TYPE_WARNING = 2;
    public static final int LOG_TYPE_ERROR = 3;
    // Request Code
    public static final int REQ_CODE_CAPTURE = 1;
    public static final int REQ_CODE_ADD_CARD = 2;
    // Task Id
    public static final int TASK_ID_REGISTER = 1;
    public static final int TASK_ID_SIGNIN = 2;
    public static final int TASK_ID_RESEND_PASSWORD = 3;
    public static final int TASK_ID_READ_DATA = 4;
    public static final int TASK_ID_UPDATE_DATA = 5;
    public static final int TASK_ID_CHANGE_PWD = 6;
    public static final int TASK_UPDATE_PROFILE = 7;
    public static final int PIC_CROP = 8;

    public static class CardData {
        public int cardId;
        public Bitmap previewImage;
        public Bitmap frontImage;
        public Bitmap backImage;
        public String cardTitle;
        public String cardNo;
        public String cardholderName;
        public String cardNote;
        /**added**/ public String category;
        public Date createdDate;
        public Date modifiedDate;
    }
    public boolean debugMode;
    public String appDirName;
    public String tempDirName;
    public String dbDirName;

    public MyLogDb myLogDb;
    public MyCardDb myCardDb;

    public boolean frontView;
    public String frontCardFilename;
    public String backCardFilename;
    public String thumbFrontCardFilename;
    public String thumbBackCardFilename;
    public ArrayList<String> cardTitles;
    public Bitmap cardImage;
    public CardData cardData;

    public int displayWidth;
    public int displayHeight;
    public String deviceId;
    public MyXMPPClient xmppClient;
    public String myUserId;
    public String myPassword;
    public String myName;
    public String myEmail;
    public String myMobilePhone;
    public String myGender;
    public String myBirthday;
    public String myAddress;
    public String myCity;
    public String resendEmail;
    //just added for avatar images
    public String avatarFileName;
    public Bitmap avatarImage;

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            debugMode = true;
            appDirName = getExternalFilesDir(null).getAbsolutePath();
            displayWidth = getResources().getDisplayMetrics().widthPixels;
            displayHeight = (int) getResources().getDisplayMetrics().heightPixels;

            // Create temporary directory
            File tempDir = new File(appDirName, "temp");
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            tempDirName = tempDir.getAbsolutePath();

            // Create db directory
            File dbDir = new File(appDirName, "db");
            if (!dbDir.exists()) {
                dbDir.mkdirs();
            }
            // Create log database
            dbDirName = dbDir.getAbsolutePath();
            String db_filename = new File(dbDirName, "log.db").getAbsolutePath();
            myLogDb = new MyLogDb(this, db_filename, 1);
            SQLiteDatabase db = myLogDb.getWritableDatabase();
            db.close();
            // Create card database
            db_filename = new File(dbDirName, "card.db").getAbsolutePath();
            myCardDb = new MyCardDb(this, db_filename, 1);
            db = myCardDb.getWritableDatabase();
            db.close();




            frontCardFilename = new File(tempDirName, "front.jpg").getAbsolutePath();
            backCardFilename = new File(tempDirName, "back.jpg").getAbsolutePath();
            thumbFrontCardFilename = new File(tempDirName, "front_thumb.jpg").getAbsolutePath();
            thumbBackCardFilename = new File(tempDirName, "back_thumb.jpg").getAbsolutePath();
            avatarFileName = new File(tempDirName, "avatar.jpg").getAbsolutePath();
            // Get card titles
            cardTitles = myCardDb.getCardTitles();
            // Delete expired logs
            deleteExpiredLogs();

            deviceId = getDeviceId();
            xmppClient = new MyXMPPClient(this);
            myUserId = "";
            myPassword = "";
            myName = "";
            myEmail = "";
            myMobilePhone = "";
            myGender = "";
            myBirthday = "";
            myAddress = "";
            myCity = "";
            //added to initialize empty avatar image

        } catch (Exception e) {
            String error_str = (e.getMessage() == null) ? "" : e.getMessage();
            error_str = String.format("[X] Fail to start app: %s", error_str);
            Log.e(LOG_TAG, error_str);
        }
    }

    private void addLog(int log_type, String log_text) {
        try {
            Date log_time = new Date();
            String log_time_str = DateFormat.format("dd/MM/yyyy HH:mm:ss", log_time).toString();
            String str = log_time_str + " " + log_text;
            myLogDb.addLog(log_time, log_type, log_text);

        } catch (Exception e) {
            String error_str = (e.getMessage() == null) ? "" : e.getMessage();
            error_str = String.format("[X] Fail to add log: %s", error_str);
            Log.e(LOG_TAG, error_str);
        }
    }

    public void logE(String msg, Exception e) {
        String error_str = (e.getMessage() == null) ? "" : e.getMessage();
        error_str = String.format("[X] %s: %s", msg, error_str);
        Log.e(LOG_TAG, error_str);
        addLog(LOG_TYPE_ERROR, error_str);
    }

    public void logE(String msg) {
        msg = "[E] " + msg;
        Log.e(LOG_TAG, msg);
        addLog(LOG_TYPE_ERROR, msg);
    }

    public void logI(String msg) {
        msg = "[I] " + msg;
        Log.i(LOG_TAG, msg);
        addLog(LOG_TYPE_INFO, msg);
    }

    public void logW(String msg) {
        msg = "[W] " + msg;
        Log.w(LOG_TAG, msg);
        addLog(LOG_TYPE_WARNING, msg);
    }

    public void logD(String msg) {
        if (debugMode) {
            msg = "[D] " + msg;
            Log.d(LOG_TAG, msg);
            addLog(LOG_TYPE_DEBUG, msg);
        }
    }

    public void deleteTempCardFiles() {
        try {
            // Delete front card file
            File f = new File(frontCardFilename);
            if (f.exists()) {
                f.delete();
            }
            f = new File(thumbFrontCardFilename);
            if (f.exists()) {
                f.delete();
            }

            // Delete back card file
            f = new File(backCardFilename);
            if (f.exists()) {
                f.delete();
            }
            f = new File(thumbBackCardFilename);
            if (f.exists()) {
                f.delete();
            }
        } catch (Exception e) {
            logE("Fail to delete temporary card files", e);
        }
    }

    private void deleteExpiredLogs() {
        try {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.add(Calendar.DATE, -7);
                    Date exp_date = cal.getTime();
                    myLogDb.deleteExpiredLogs(exp_date);
                }
            }).start();

        } catch (Exception e) {
            logE("Fail to delete expired logs", e);
        }
    }

    public void showKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        } catch (Exception e) {
            logE("Fail to show keyboard", e);
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        } catch (Exception e) {
            logE("Fail to hide keyboard", e);
        }
    }

    public int getColor(int color_id) {
        return getResources().getColor(color_id);
    }

    // Convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        }
        return stream.toByteArray();
    }

    // Convert from byte array to bitmap
    public static Bitmap getBitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    private String getDeviceId() {
        try {
            TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            return mgr.getDeviceId();

        } catch (Exception e) {
            logE("Fail to get device id", e);
            return "";
        }
    }
}
