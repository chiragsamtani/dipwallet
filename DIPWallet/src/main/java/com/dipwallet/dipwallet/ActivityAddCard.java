package com.dipwallet.dipwallet;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;


public class ActivityAddCard extends ActionBarActivity {

    private final String LOG_TAG = "ActivityAddCard";

    MyApp myApp;
    TextView textViewType;
    ImageView imageCard;
    ImageButton buttonSwapView;
    ImageButton buttonCapture;
    ImageButton buttonDeleteCardImage;
    AutoCompleteTextView autoCardTitle;
    EditText editCardNo;
    EditText editCardholderName;
    EditText editCardNote;
    Button buttonCancelAddCard;
    Button buttonOkAddCard;
    int displayWidth;
    int displayHeight;
    String category_name;


    Spinner spinnerCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = (MyApp) getApplicationContext();
        myApp.frontView = true;

        setContentView(R.layout.activity_add_card);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Activity activity = this;


//        ActionBar action_bar = getActionBar();
        // Setup the action bar
//        action_bar.setDisplayShowHomeEnabled(true);
//        action_bar.setDisplayHomeAsUpEnabled(true);
//        action_bar.setHomeButtonEnabled(true);

        textViewType = (TextView) findViewById(R.id.text_view_type);

        imageCard = (ImageView) findViewById(R.id.imageCard);
        imageCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCardImage();
            }
        });

        displayWidth = (int) getResources().getDimension(R.dimen.image_view_width);
        displayHeight = (int) getResources().getDimension(R.dimen.image_view_height);

        buttonSwapView = (ImageButton) findViewById(R.id.button_swap_view);
        buttonSwapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swapView();
            }
        });

        buttonCapture = (ImageButton) findViewById(R.id.button_capture);
        buttonCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureCard();
            }
        });

        buttonDeleteCardImage = (ImageButton) findViewById(R.id.button_delete_card);
        buttonDeleteCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCardImage();
            }
        });

        autoCardTitle = (AutoCompleteTextView) findViewById(R.id.auto_card_title);
        AdapterCardTitle adapter = new AdapterCardTitle(this, R.layout.adapter_card_title);
        autoCardTitle.setAdapter(adapter);
        editCardNo = (EditText) findViewById(R.id.edit_card_no);
        editCardholderName = (EditText) findViewById(R.id.edit_cardholder_name);
        editCardNote = (EditText) findViewById(R.id.edit_card_note);

        spinnerCategory = (Spinner)findViewById(R.id.edit_category);
        ArrayAdapter spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.category, R.layout.spinner_layout);
        spinnerCategory.setAdapter(spinnerAdapter);

        buttonCancelAddCard = (Button) findViewById(R.id.button_cancel_add_card);
        buttonCancelAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonOkAddCard = (Button) findViewById(R.id.button_ok_add_card);
        buttonOkAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCard();
                finish();
            }
        });

        myApp.deleteTempCardFiles();
        showEmptyView();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_add_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home: finish(); return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if(resultCode == MyApp.REQ_CODE_CAPTURE){
                boolean success = data.getBooleanExtra("success", false);
                if(success){
                    if(myApp.frontView){
                        String frontFileName = myApp.frontCardFilename;
                        File file = new File(frontFileName);
                        if(file.exists()){
                            Uri uri = Uri.fromFile(file);
                            performCrop(uri);
                        }
                    }
                    else{
                        String backFileName = myApp.backCardFilename;
                        File file = new File(backFileName);
                        if(file.exists()){
                            Uri uri = Uri.fromFile(file);
                            performCrop(uri);
                        }
                    }
                }else{
                    showEmptyView();
                }
            }
            else if(resultCode == MyApp.PIC_CROP) {
                boolean result = data.getBooleanExtra("crop", false);
                if (result) {
                    if (myApp.frontView) {
                        showFront();
                    } else {
                        showBack();
                    }

                }
            }

        } catch (Exception e) {
            myApp.logE("Fail on get activity result", e);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void performCrop(Uri picUri){
        Intent intent = new Intent(this, CropCardImage.class);
        intent.putExtra("imageUri", picUri);
        startActivityForResult(intent, MyApp.PIC_CROP);

    }
    private void logE(String msg, Exception e) {
        String error = (e.getMessage() == null) ? "" : e.getMessage();
        Log.e(LOG_TAG, String.format("%s: %s", msg, error));
    }

    private void captureCard() {
        Intent intent = new Intent(this, ActivityCaptureCard.class);
        startActivityForResult(intent, MyApp.REQ_CODE_CAPTURE);
    }

    private void showEmptyView() {
        try {
            // Clear image
            imageCard.setImageDrawable(null);
            // Draw cross line
            Bitmap bmp = Bitmap.createBitmap(displayWidth, displayHeight, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bmp);
            imageCard.draw(c);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setColor(getResources().getColor(R.color.DimGray));
            c.drawLine(0, 0, displayWidth, displayHeight, p);
            c.drawLine(0, displayHeight, displayWidth, 0, p);
            imageCard.setImageBitmap(bmp);

        } catch (Exception e) {
            myApp.logE("Fail to show empty view", e);
        }
    }

    private void showFront() {
        try {
            myApp.frontView = true;
            textViewType.setText(R.string.front);
            String filename = myApp.frontCardFilename;
            File file = new File(filename);
            if (file.exists()) {
                Bitmap bmp = decodeFile(file, displayWidth, displayHeight);
                imageCard.setImageBitmap(bmp);
            } else {
                showEmptyView();
            }

        } catch (Exception e) {

        }
    }

    private void showBack() {
        try {
            myApp.frontView = false;
            textViewType.setText(R.string.back);
            String filename = myApp.backCardFilename;
            File file = new File(filename);
            if (file.exists()) {
                Bitmap bmp = decodeFile(file, displayWidth, displayHeight);
                imageCard.setImageBitmap(bmp);
            } else {
                showEmptyView();
            }

        } catch (Exception e) {

        }
    }

    private void swapView() {
        try {
            if (myApp.frontView) {
                showBack();
            } else {
                showFront();
            }

        } catch (Exception e) {

        }

    }

    private void showCardImage() {
        try {
            myApp.cardImage = null;
            String filename = (myApp.frontView) ? myApp.frontCardFilename : myApp.backCardFilename;
            File file = new File(filename);
            if (file.exists()) {
                ImageView frame = (ImageView) findViewById(R.id.image_card_full);

                myApp.cardImage = decodeFile(file, displayWidth, displayHeight);
                // myApp.cardImage = BitmapFactory.decodeFile(filename);
                Intent intent = new Intent(this, ActivityViewCardImage.class);
                startActivity(intent);
            }

        } catch (Exception e) {
            myApp.logE("Fail to show card image", e);
        }
    }

    private void deleteCardImage() {
        try {
            String filename = (myApp.frontView) ? myApp.frontCardFilename : myApp.backCardFilename;
            final File file = new File(filename);
            if (file.exists()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.delete_image_confirm);
                builder.setMessage(R.string.delete_image_action);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (file.delete()) {
                            showEmptyView();
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();

            } else {
                showEmptyView();
            }

        } catch (Exception e) {
            myApp.logE("Fail to show card image", e);
        }
    }

    private void addCard() {
        try {
            // Get preview image
            String preview_image_filename = myApp.thumbFrontCardFilename;
            File preview_image_file = new File(preview_image_filename);
            Bitmap preview_image = null;
            if (preview_image_file.exists()) {
                preview_image = BitmapFactory.decodeFile(preview_image_filename);
            }
            // Get front image
            String front_image_filename = myApp.frontCardFilename;
            File front_image_file = new File(front_image_filename);
            Bitmap front_image = null;
            if (front_image_file.exists()) {
                int height = (int)getResources().getDimension(R.dimen.view_photo_size_height);
                int width = (int)getResources().getDimension(R.dimen.view_photo_size_width);
                front_image = decodeFile(front_image_file, width, height);
            }
            // Get back image
            String back_image_filename = myApp.backCardFilename;
            File back_image_file = new File(back_image_filename);
            Bitmap back_image = null;
            if (back_image_file.exists()) {
                int height = (int)getResources().getDimension(R.dimen.view_photo_size_height);
                int width = (int)getResources().getDimension(R.dimen.view_photo_size_width);
                back_image = decodeFile(back_image_file, width, height);
            }

            String card_title =  autoCardTitle.getText().toString();


            String card_no = editCardNo.getText().toString();
            String cardholder_name = editCardholderName.getText().toString();
            String card_note = editCardNote.getText().toString();
            /**new Spinner Stuff**/
            category_name = String.valueOf(spinnerCategory.getSelectedItem());


            myApp.cardData = new MyApp.CardData();
            myApp.cardData.previewImage = preview_image;
            myApp.cardData.frontImage = front_image;
            myApp.cardData.backImage = back_image;
            myApp.cardData.cardTitle = card_title;
            myApp.cardData.cardNo = card_no;
            myApp.cardData.cardholderName = cardholder_name;
            myApp.cardData.cardNote = card_note;
            /**spinner stuff**/
            myApp.cardData.category = category_name;


            boolean result = myApp.myCardDb.addCard(myApp.cardData);
            setActivityResult(result);

        } catch (Exception e) {
            setActivityResult(false);
            logE("Fail to add card", e);
        }
    }

    private void setActivityResult(boolean success) {
        Intent data = new Intent();
        data.putExtra("success", success);
        data.putExtra("category", category_name);
        setResult(MyApp.REQ_CODE_ADD_CARD, data);
    }

    private static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}